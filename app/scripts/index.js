/*global require*/
'use strict';

require.config({
    paths: {
        jquery: '/static/scripts/vendor/jquery',
        backbone: '/static/scripts/vendor/backbone',
        underscore: '/static/scripts/vendor/lodash',
        bootstrap: '/static/scripts/vendor/bootstrap',
        modernizr: '/static/scripts/vendor/modernizr',
        KeyboardJS: '/static/scripts/vendor/keyboard',
        handlebars: '/static/scripts/vendor/handlebars',
        mustache: '/static/scripts/vendor/mustache',
        geometry: '/static/scripts/vendor/geometry',
        Vectorizer: '/static/scripts/vendor/vectorizer',
        joint: '/static/scripts/vendor/joint',
        fabric: '/static/scripts/vendor/fabric',
        FabricEditor: '/static/scripts/fabric_editor',
        MDConverter: '/static/scripts/vendor/Markdown.Converter',
        MDEditor: '/static/scripts/vendor/Markdown.Editor',
        MDExtra: '/static/scripts/vendor/Markdown.Extra',
        MDSanitizer: '/static/scripts/vendor/Markdown.Sanitizer'
    },    
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        },
        modernizr: {
            exports: 'Modernizr'
        },
        MDConverter: {
            exports: 'Markdown'
        },
        MDEditor: {
            deps: ['MDConverter']
        },
        MDExtra: {
            deps: ['MDConverter']
        },
        MDSanitizer: {
            deps: ['MDConverter']
        }
        
//        joint: {
//            deps: 'KeyboardJS',
//            exports: 'joint'
//        }
    }
});

require(['jquery', 'underscore', 'backbone', 'bootstrap', 'KeyboardJS', 'handlebars', 'mustache', 'joint', 'fabric', 'FabricEditor', 'MDConverter', 'MDEditor', 'MDExtra', 'MDSanitizer'], function ($, _, Backbone, bootstrap, KeyboardJS, Handlebars, Mustache, joint, fabric, EditorView, Markdown, MDEditor, MDExtra, MDSanitizer) {

var editorView = new EditorView();
$('#fabricModal .modal-body').append(editorView.el);

//////////////////////////////////////// Inspector configuration

var CommonInspectorInputs = {

    size: {
        width: { type: 'number', min: 1, max: 500, group: 'geometry', label: 'width', index: 1 },
        height: { type: 'number', min: 1, max: 500, group: 'geometry', label: 'height', index: 2 }
    },
    position: {
        x: { type: 'number', min: 1, max: 2000, group: 'geometry', label: 'x', index: 3 },
        y: { type: 'number', min: 1, max: 2000, group: 'geometry', label: 'y', index: 4 }
    },
    custom: { type: 'text', group: 'data', index: 1, label: 'Custom data', attrs: { 'label': { 'data-tooltip': 'An example of setting custom data via Inspector.' } } }
};

var CommonInspectorGroups = {

    text: { label: 'Text', index: 1 },
    presentation: { label: 'Presentation', index: 2 },
    geometry: { label: 'Geometry', index: 3 },
    data: { label: 'Data', index: 4 }
};

var CommonInspectorTextInputs = {
    'text': { type: 'textarea', group: 'text', index: 1 },
    'font-size': { type: 'range', min: 5, max: 80, unit: 'px', group: 'text', index: 2 },
    'font-family': { type: 'select', options: ['Arial', 'Helvetica', 'Times New Roman', 'Courier New', 'Georgia', 'Garamond', 'Tahoma', 'Lucida Console', 'Comic Sans MS'], group: 'text', index: 3 },
    'font-weight': { type: 'range', min: 100, max: 900, step: 100, defaultValue: 400, group: 'text', index: 4 },
    'fill': { type: 'color', group: 'text', index: 5 },
    'stroke': { type: 'color', group: 'text', index: 6, defaultValue: '#000000' },
    'stroke-width': { type: 'range', min: 0, max: 5, step: .5, defaultValue: 0, unit: 'px', group: 'text', index: 7 },
    'ref-x': { type: 'range', min: 0, max: .9, step: .1, defaultValue: .5, group: 'text', index: 8 },
    'ref-y': { type: 'range', min: 0, max: .9, step: .1, defaultValue: .5, group: 'text', index: 9 }
};

var InputDefs = {
    text: { type: 'textarea', label: 'Text' },
    'font-size': { type: 'range', min: 5, max: 80, unit: 'px', label: 'Font size' },
    'font-family': { type: 'select', options: ['Arial', 'Helvetica', 'Times New Roman', 'Courier New', 'Georgia', 'Garamond', 'Tahoma', 'Lucida Console', 'Comic Sans MS'], label: 'Font family' },
    'font-weight': { type: 'range', min: 100, max: 900, step: 100, defaultValue: 400, label: 'Font weight' },
    'fill': { type: 'color', label: 'Fill color' },
    'stroke': { type: 'color', defaultValue: '#000000', label: 'Stroke' },
    'stroke-width': { type: 'range', min: 0, max: 5, step: .5, defaultValue: 0, unit: 'px', label: 'Stroke width' },
    'ref-x': { type: 'range', min: 0, max: .9, step: .1, defaultValue: .5, label: 'Horizontal alignment' },
    'ref-y': { type: 'range', min: 0, max: .9, step: .1, defaultValue: .5, label: 'Vertical alignment' },
    'ref-dx': { type: 'range', min: 0, max: 50, step: 1, defaultValue: 0, label: 'Horizontal offset' },
    'ref-dy': { type: 'range', min: 0, max: 50, step: 1, defaultValue: 0, label: 'Vertical offset' },
    'dx': { type: 'range', min: 0, max: 50, step: 1, defaultValue: 0, label: 'Horizontal distance' },
    'dy': { type: 'range', min: 0, max: 50, step: 1, defaultValue: 0, label: 'Vertical distance' },
    'stroke-dasharray': { type: 'select', options: ['0', '1', '5,5', '5,10', '10,5', '3,5', '5,1', '15,10,5,10,15'], label: 'Stroke dasharray' },
    rx: { type: 'range', min: 0, max: 30, defaultValue: 1, unit: 'px', label: 'X-axis radius' },
    ry: { type: 'range', min: 0, max: 30, defaultValue: 1, unit: 'px', label: 'Y-axis radius' },
    'xlink:href': { type: 'text', label: 'Image URL' }
};

function inp(defs) {
    var ret = {};
    _.each(defs, function(def, attr) {

        ret[attr] = _.extend({}, InputDefs[attr], def);
    });
    return ret;
}

var InspectorDefs = {

    'link': {

        inputs: {
            attrs: {
                '.connection': {
                    'stroke-width': { type: 'range', min: 0, max: 50, defaultValue: 1, unit: 'px', group: 'connection', label: 'stroke width', index: 1 },
                    'stroke': { type: 'color', group: 'connection', label: 'stroke color', index: 2 },
                    'stroke-dasharray': { type: 'select', options: ['0', '1', '5,5', '5,10', '10,5', '5,1', '15,10,5,10,15'], group: 'connection', label: 'stroke dasharray', index: 3 }
                },
                '.marker-source': {
                    transform: { type: 'range', min: 1, max: 15, unit: 'x scale', defaultValue: 'scale(1)', valueRegExp: '(scale\\()(.*)(\\))', group: 'marker-source', label: 'source arrowhead size', index: 1 },
                    fill: { type: 'color', group: 'marker-source', label: 'soure arrowhead color', index: 2 }
                },
                '.marker-target': {
                    transform: { type: 'range', min: 1, max: 15, unit: 'x scale', defaultValue: 'scale(1)', valueRegExp: '(scale\\()(.*)(\\))', group: 'marker-target', label: 'target arrowhead size', index: 1 },
                    fill: { type: 'color', group: 'marker-target', label: 'target arrowhead color', index: 2 }
                }
            },
            smooth: { type: 'toggle', group: 'connection', index: 4 },
            manhattan: { type: 'toggle', group: 'connection', index: 5 },
            labels: {
                type: 'list',
                group: 'labels',
                attrs: {
                    label: { 'data-tooltip': 'Set (possibly multiple) labels for the link' }
                },
                item: {
                    type: 'object',
                    properties: {
                        position: { type: 'range', min: 0.1, max: .9, step: .1, defaultValue: .5, label: 'position', index: 2, attrs: { label: { 'data-tooltip': 'Position the label relative to the source of the link' } } },
                        attrs: {
                            text: {
                                text: { type: 'text', label: 'text', defaultValue: 'label', index: 1, attrs: { label: { 'data-tooltip': 'Set text of the label' } } }
                            }
                        }
                    }
                }
            }

        },
        groups: {
            labels: { label: 'Labels', index: 1 },
            'connection': { label: 'Connection', index: 2 },
            'marker-source': { label: 'Source marker', index: 3 },
            'marker-target': { label: 'Target marker', index: 4 }
        }
    },

    // Basic
    // -----

    'basic.Rect': {

        inputs: _.extend({
            attrs: {
                text: inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                rect: inp({
                    fill: { group: 'presentation', index: 1 },
                    'stroke-width': { group: 'presentation', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'presentation', index: 3 },
                    rx: { group: 'presentation', index: 4 },
                    ry: { group: 'presentation', index: 5 }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },
    
    'basic.Circle': {

        inputs: _.extend({
            attrs: {
                text: inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                circle: inp({
                    fill: { group: 'presentation', index: 1 },
                    'stroke-width': { group: 'presentation', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { type: 'select', options: ['0', '1', '5,5', '5,10', '10,5', '5,1', '15,10,5,10,15'], group: 'presentation', index: 3 }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },
    
    'basic.Image': {

        inputs: _.extend({
            attrs: {
                text: inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-dy': { group: 'text', index: 9 }
                }),
                image: inp({
                    'xlink:href': { group: 'presentation', index: 1 }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },

    // DEVS
    // ----
    
    'devs.Atomic': {
        
        inputs: _.extend({
            attrs: {
                '.label': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9, min: 0, max: 30, step: 1 }
                }),
                rect: inp({
                    fill: { group: 'presentation', index: 1 },
                    'stroke-width': { min: 0, max: 30, defaultValue: 1, unit: 'px', group: 'presentation', index: 2 },
                    'stroke-dasharray': { group: 'presentation', index: 3 },
                    'rx': { group: 'presentation', index: 4 },
                    'ry': { group: 'presentation', index: 5 }
                }),
                '.inPorts circle': inp({
                    fill: { group: 'presentation', index: 6, label: 'Input ports fill color' }
                }),
                '.outPorts circle': inp({
                    fill: { group: 'presentation', index: 7, label: 'Output ports fill color' }
                })
            },
            inPorts: { type: 'list', item: { type: 'text' }, group: 'data', index: -2 },
            outPorts: { type: 'list', item: { type: 'text' }, group: 'data', index: -1 }
            
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },

    // FSA
    // ---

    'fsa.StartState': {

        inputs: _.extend({
            attrs: {
                circle: inp({
                    fill: { group: 'presentation', index: 1 }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },

    'fsa.EndState': {

        inputs: _.extend({
            attrs: {
                '.outer': inp({
                    fill: { group: 'presentation', index: 1, label: 'Outer circle fill color' },
                    'stroke-dasharray': { group: 'presentation', index: 2, label: 'Outer circle stroke dasharray' }
                }),
                '.inner': inp({
                    fill: { group: 'presentation', index: 3, label: 'Inner circle fill color' }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },
    
    'fsa.State': {

        inputs: _.extend({
            attrs: {
                text: inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                circle: inp({
                    fill: { group: 'presentation', index: 1 },
                    'stroke-width': { group: 'presentation', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { type: 'select', options: ['0', '1', '5,5', '5,10', '10,5', '5,1', '15,10,5,10,15'], group: 'presentation', index: 3 }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },

    // PN
    // --
    
    'pn.Place': {

        inputs: _.extend({
            attrs: {
                '.label': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9, min: -50, max: 0, step: 1 }
                }),
                '.root': inp({
                    fill: { group: 'presentation', index: 1 },
                    'stroke-width': { group: 'presentation', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { type: 'select', options: ['0', '1', '5,5', '5,10', '10,5', '5,1', '15,10,5,10,15'], group: 'presentation', index: 3 }
                })
            },
            tokens: { type: 'number', min: 1, max: 500, group: 'data', index: 1 }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },

    'pn.Transition': {

        inputs: _.extend({
            attrs: {
                '.label': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9, min: -50, max: 0, step: 1 }
                }),
                rect: inp({
                    fill: { group: 'presentation', index: 1 },
                    'stroke-width': { group: 'presentation', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'presentation', index: 3 },
                    rx: { group: 'presentation', index: 4 },
                    ry: { group: 'presentation', index: 5 }
                })
            }
        }, CommonInspectorInputs),

        groups: CommonInspectorGroups
    },

    // ERD
    // ---

    'erd.Entity': {

        inputs: _.extend({
            attrs: {
                'text': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                '.outer': inp({
                    fill: { group: 'presentation', index: 1 },
                    stroke: { group: 'presentation', index: 2 },
                    'stroke-width': { group: 'presentation', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'presentation', index: 4 }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },
    
    'erd.WeakEntity': {

        inputs: _.extend({
            attrs: {
                'text': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                '.outer': inp({
                    fill: { group: 'outer', index: 1 },
                    stroke: { group: 'outer', index: 2 },
                    'stroke-width': { group: 'outer', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'outer', index: 4 }
                }),
                '.inner': inp({
                    fill: { group: 'inner', index: 1 },
                    stroke: { group: 'inner', index: 2 },
                    'stroke-width': { group: 'inner', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'inner', index: 4 }
                })
            }
        }, CommonInspectorInputs),
        groups: {
            text: { label: 'Text', index: 1 },
            outer: { label: 'Outer polygon', index: 2 },
            inner: { label: 'Inner polygon', index: 3 },
            geometry: { label: 'Geometry', index: 4 },
            data: { label: 'Data', index: 5 }
        }
    },

    'erd.Relationship': {

        inputs: _.extend({
            attrs: {
                'text': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                '.outer': inp({
                    fill: { group: 'outer', index: 1 },
                    stroke: { group: 'outer', index: 2 },
                    'stroke-width': { group: 'outer', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'outer', index: 4 }
                }),
                '.inner': inp({
                    fill: { group: 'inner', index: 1 },
                    stroke: { group: 'inner', index: 2 },
                    'stroke-width': { group: 'inner', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'inner', index: 4 }
                })
            }
        }, CommonInspectorInputs),
        groups: {
            text: { label: 'Text', index: 1 },
            outer: { label: 'Outer polygon', index: 2 },
            inner: { label: 'Inner polygon', index: 3 },
            geometry: { label: 'Geometry', index: 4 },
            data: { label: 'Data', index: 5 }
        }
    },
    
    'erd.IdentifyingRelationship': {
        
        inputs: _.extend({
            attrs: {
                'text': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                '.outer': inp({
                    fill: { group: 'outer', index: 1 },
                    stroke: { group: 'outer', index: 2 },
                    'stroke-width': { group: 'outer', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'outer', index: 4 }
                }),
                '.inner': inp({
                    fill: { group: 'inner', index: 1 },
                    stroke: { group: 'inner', index: 2 },
                    'stroke-width': { group: 'inner', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'inner', index: 4 }
                })
            }
        }, CommonInspectorInputs),
        groups: {
            text: { label: 'Text', index: 1 },
            outer: { label: 'Outer polygon', index: 2 },
            inner: { label: 'Inner polygon', index: 3 },
            geometry: { label: 'Geometry', index: 4 },
            data: { label: 'Data', index: 5 }
        }
    },

    'erd.Key': {

        inputs: _.extend({
            attrs: {
                'text': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                '.outer': inp({
                    fill: { group: 'presentation', index: 1 },
                    stroke: { group: 'presentation', index: 2 },
                    'stroke-width': { group: 'presentation', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'presentation', index: 4 }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },
    
    'erd.Normal': {

        inputs: _.extend({
            attrs: {
                'text': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                '.outer': inp({
                    fill: { group: 'presentation', index: 1 },
                    stroke: { group: 'presentation', index: 2 },
                    'stroke-width': { group: 'presentation', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'presentation', index: 4 }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },

    'erd.Multivalued': {

        inputs: _.extend({
            attrs: {
                'text': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                '.outer': inp({
                    fill: { group: 'outer', index: 1 },
                    stroke: { group: 'outer', index: 2 },
                    'stroke-width': { group: 'outer', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'outer', index: 4 }
                }),
                '.inner': inp({
                    fill: { group: 'inner', index: 1 },
                    stroke: { group: 'inner', index: 2 },
                    'stroke-width': { group: 'inner', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'inner', index: 4 }
                })
            }
        }, CommonInspectorInputs),
        groups: {
            text: { label: 'Text', index: 1 },
            outer: { label: 'Outer ellipse', index: 2 },
            inner: { label: 'Inner ellipse', index: 3 },
            geometry: { label: 'Geometry', index: 4 },
            data: { label: 'Data', index: 5 }
        }
    },

    'erd.Derived': {

        inputs: _.extend({
            attrs: {
                'text': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                '.outer': inp({
                    fill: { group: 'presentation', index: 1 },
                    stroke: { group: 'presentation', index: 2 },
                    'stroke-width': { group: 'presentation', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'presentation', index: 4 }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },

    'erd.ISA': {

        inputs: _.extend({
            attrs: {
                'text': inp({
                    text: { group: 'text', index: 1 },
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9 }
                }),
                'polygon': inp({
                    fill: { group: 'presentation', index: 1 },
                    stroke: { group: 'presentation', index: 2 },
                    'stroke-width': { group: 'presentation', index: 3, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'presentation', index: 4 }
                })
            }
        }, CommonInspectorInputs),
        groups: CommonInspectorGroups
    },

    // UML
    // ---

    'uml.Class': {

        inputs: _.extend({
            attrs: {
                '.uml-class-name-text': inp({
                    'font-size': { group: 'name', index: 2 },
                    'font-family': { group: 'name', index: 3 }
                }),
                '.uml-class-attrs-text': inp({
                    'font-size': { group: 'attributes', index: 2 },
                    'font-family': { group: 'attributes', index: 3 }
                }),
                '.uml-class-methods-text': inp({
                    'font-size': { group: 'methods', index: 2 },
                    'font-family': { group: 'methods', index: 3 }
                }),
                '.uml-class-name-rect': inp({
                    fill: { group: 'name', index: 4 },
                    'stroke-width': { group: 'name', index: 5, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'name', index: 6 },
                    rx: { group: 'name', index: 7 },
                    ry: { group: 'name', index: 8 }
                }),
                '.uml-class-attrs-rect': inp({
                    fill: { group: 'attributes', index: 4 },
                    'stroke-width': { group: 'attributes', index: 5, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'attributes', index: 6 },
                    rx: { group: 'attributes', index: 7 },
                    ry: { group: 'attributes', index: 8 }
                }),
                '.uml-class-methods-rect': inp({
                    fill: { group: 'methods', index: 4 },
                    'stroke-width': { group: 'methods', index: 5, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'methods', index: 6 },
                    rx: { group: 'methods', index: 7 },
                    ry: { group: 'methods', index: 8 }
                })
            },
            name: { type: 'text', group: 'name', index: 1, label: 'Class name' },
            attributes: { type: 'list', item: { type: 'text' }, group: 'attributes', index: 1, label: 'Attributes' },
            methods: { type: 'list', item: { type: 'text' }, group: 'methods', index: 1, label: 'Methods' }
        }, CommonInspectorInputs),
        groups: {
            name: { label: 'Class name', index: 1 },
            attributes: { label: 'Attributes', index: 2 },
            methods: { label: 'Methods', index: 3 },
            geometry: { label: 'Geometry', index: 4 },
            data: { label: 'Data', index: 5 }
        }
    },
    
    'uml.Interface': {

        inputs: _.extend({
            attrs: {
                '.uml-class-name-rect': inp({
                    fill: { group: 'name', index: 1 },
                    'stroke-width': { group: 'name', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'name', index: 3 },
                    rx: { group: 'name', index: 4 },
                    ry: { group: 'name', index: 5 }
                }),
                '.uml-class-attrs-rect': inp({
                    fill: { group: 'attributes', index: 1 },
                    'stroke-width': { group: 'attributes', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'attributes', index: 3 },
                    rx: { group: 'attributes', index: 4 },
                    ry: { group: 'attributes', index: 5 }
                }),
                '.uml-class-methods-rect': inp({
                    fill: { group: 'methods', index: 1 },
                    'stroke-width': { group: 'methods', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'methods', index: 3 },
                    rx: { group: 'methods', index: 4 },
                    ry: { group: 'methods', index: 5 }
                })
            },
            name: { type: 'text', group: 'name', index: 0, label: 'Interface name' },
            attributes: { type: 'list', item: { type: 'text' },  group: 'attributes', index: 0, label: 'Attributes' },
            methods: { type: 'list', item: { type: 'text' }, group: 'methods', index: 0, label: 'Methods' }
        }, CommonInspectorInputs),
        groups: {
            name: { label: 'Interface name', index: 1 },
            attributes: { label: 'Attributes', index: 2 },
            methods: { label: 'Methods', index: 3 },
            geometry: { label: 'Geometry', index: 4 },
            data: { label: 'Data', index: 5 }
        }
    },

    'uml.Abstract': {

        inputs: _.extend({
            attrs: {
                '.uml-class-name-rect': inp({
                    fill: { group: 'name', index: 1 },
                    'stroke-width': { group: 'name', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'name', index: 3 },
                    rx: { group: 'name', index: 4 },
                    ry: { group: 'name', index: 5 }
                }),
                '.uml-class-attrs-rect': inp({
                    fill: { group: 'attributes', index: 1 },
                    'stroke-width': { group: 'attributes', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'attributes', index: 3 },
                    rx: { group: 'attributes', index: 4 },
                    ry: { group: 'attributes', index: 5 }
                }),
                '.uml-class-methods-rect': inp({
                    fill: { group: 'methods', index: 1 },
                    'stroke-width': { group: 'methods', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'methods', index: 3 },
                    rx: { group: 'methods', index: 4 },
                    ry: { group: 'methods', index: 5 }
                })
            },
            name: { type: 'text', group: 'name', index: 0, label: 'Abstract class name' },
            attributes: { type: 'list', item: { type: 'text' }, group: 'attributes', index: 0, label: 'Attributes' },
            methods: { type: 'list', item: { type: 'text' }, group: 'methods', index: 0, label: 'Methods' }
        }, CommonInspectorInputs),
        groups: {
            name: { label: 'Abstract class name', index: 1 },
            attributes: { label: 'Attributes', index: 2 },
            methods: { label: 'Methods', index: 3 },
            geometry: { label: 'Geometry', index: 4 },
            data: { label: 'Data', index: 5 }
        }
    },

    'uml.State': {

        inputs: _.extend({
            name: { group: 'text', index: 1, type: "text" },
            events: { group: 'events', index: 1, type: "list", item: { type: "text" }},
            attrs: {
                '.uml-state-name': inp({
                    'font-size': { group: 'text', index: 2 },
                    'font-family': { group: 'text', index: 3 },
                    'font-weight': { group: 'text', index: 4 },
                    fill: { group: 'text', index: 5 },
                    stroke: { group: 'text', index: 6 },
                    'stroke-width': { group: 'text', index: 7 },
                    'ref-x': { group: 'text', index: 8 },
                    'ref-y': { group: 'text', index: 9, min: -20, max: 20, step: 1 }
                }),
                '.uml-state-body': inp({
                    fill: { group: 'presentation', index: 1 },
                    stroke: { group: 'presentation', index: 2 },
                    'stroke-width': { group: 'presentation', index: 4, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'presentation', index: 5 },
                    rx: { group: 'presentation', index: 6 },
                    ry: { group: 'presentation', index: 7 }
                }),
                '.uml-state-separator': inp({
                    stroke: { group: 'presentation', index: 3, label: 'Horizontal rule stroke color' }
                }),
                '.uml-state-events': inp({
                    'font-size': { group: 'events', index: 2 },
                    'font-family': { group: 'events', index: 3 },
                    'font-weight': { group: 'events', index: 4 },
                    fill: { group: 'events', index: 5 },
                    stroke: { group: 'events', index: 6 },
                    'stroke-width': { group: 'events', index: 7 },
                    'ref-x': { group: 'events', index: 8, min: -20, max: 20, step: 1 },
                    'ref-y': { group: 'events', index: 9, min: -20, max: 20, step: 1 }
                }),
            },
        }, CommonInspectorInputs),
        groups: {
            text: { label: 'State name text', index: 1 },
            events: { label: 'State events text', index: 2 },
            presentation: { label: 'Presentation', index: 3 },
            geometry: { label: 'Geometry', index: 4 },
            data: { label: 'Data', index: 5 }
        }
    },

    // Org
    // ---

    'org.Member': {
        
        inputs: _.extend({
            attrs: {
                '.rank': inp({
                    text: { group: 'rank', index: 1 },
                    'font-size': { group: 'rank', index: 2 },
                    'font-family': { group: 'rank', index: 3 },
                    'font-weight': { group: 'rank', index: 4 },
                    fill: { group: 'rank', index: 5 },
                    stroke: { group: 'rank', index: 6 },
                    'stroke-width': { group: 'rank', index: 7 },
                    'ref-x': { group: 'rank', index: 8 },
                    'ref-y': { group: 'rank', index: 9 }
                }),
                '.name': inp({
                    text: { group: 'name', index: 1 },
                    'font-size': { group: 'name', index: 2 },
                    'font-family': { group: 'name', index: 3 },
                    'font-weight': { group: 'name', index: 4 },
                    fill: { group: 'name', index: 5 },
                    stroke: { group: 'name', index: 6 },
                    'stroke-width': { group: 'name', index: 7 },
                    'ref-x': { group: 'name', index: 8 },
                    'ref-y': { group: 'name', index: 9 }
                }),
                '.card': inp({
                    fill: { group: 'presentation', index: 1 },
                    'stroke-width': { group: 'presentation', index: 2, min: 0, max: 30, defaultValue: 1 },
                    'stroke-dasharray': { group: 'presentation', index: 3 },
                    rx: { group: 'presentation', index: 4 },
                    ry: { group: 'presentation', index: 5 }
                }),
                image: inp({
                    'xlink:href': { group: 'photo', index: 1 }
                })
            }
        }, CommonInspectorInputs),
        groups: {
            rank: { label: 'Rank', index: 1 },
            name: { label: 'Name', index: 2 },
            photo: { label: 'Photo', index: 3 },
            presentation: { label: 'Presentation', index: 4 },
            geometry: { label: 'Geometry', index: 5 },
            data: { label: 'Data', index: 6 }
        }
    }
};


//////////////////////////////////////// Stencil configuration

var Stencil = {};

Stencil.groups = {
    basic: { index: 1, label: 'Basic shapes' },
    fsa: { index: 2, label: 'State machine' },
    pn: { index: 3, label: 'Petri nets' },
    erd: { index: 4, label: 'Entity-relationship' },
    uml: { index: 5, label: 'UML' },
    org: { index: 6, label: 'ORG' }
};

Stencil.shapes = {

    basic: [
        new joint.shapes.basic.Rect({
            size: { width: 5, height: 3 },
            attrs: {
                rect: {
                    rx: 2, ry: 2, width: 50, height: 30,
                    fill: '#27AE60'
                },
                text: { text: 'rect', fill: '#ffffff', 'font-size': 10, stroke: '#000000', 'stroke-width': 0 }
            }
        }),
        new joint.shapes.basic.Circle({
            size: { width: 5, height: 3 },
            attrs: {
                circle: { width: 50, height: 30, fill: '#E74C3C' },
                text: { text: 'ellipse', fill: '#ffffff', 'font-size': 10, stroke: '#000000', 'stroke-width': 0 }
            }
        }),
        new joint.shapes.devs.Atomic({
            size: { width: 4, height: 3 },
            inPorts: ['in1','in2'],
            outPorts: ['out'],
            attrs: {
            rect: { fill: '#8e44ad', rx: 2, ry: 2 },
                '.label': { text: 'model', fill: '#ffffff', 'font-size': 10, stroke: '#000000', 'stroke-width': 0 },
            '.inPorts circle': { fill: '#f1c40f', opacity: 0.9 },
                '.outPorts circle': { fill: '#f1c40f', opacity: 0.9 },
            '.inPorts text, .outPorts text': { 'font-size': 9 }
            }
        }),
        new joint.shapes.basic.Image({
            attrs: {
                image: { width: 50, height: 50, 'xlink:href': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAIj0lEQVRogd2Za2wcVxXHf3f2vXayttd52o3yJK9GcpMUNQQIfQVKW6mK1ApVqgQigEBCQghVQUKtKiHBR6j4wEMIofKppRRVgAKl0FJISmijJqTNw2netuvY8Xu9692Ze/gwrzuzu46dIIdypNHM3Lnn3P//PO69MwMfclEN7pNAGrAWHs6sooEqYAPiN5oEksDinu/2fr/rtuX7crn04jp6noLZrGJ9/Pu4qj+iSKxdon1ij4MH5XJ1ou9y/2/e+c7GA8A44JjjWEBx49OnflX8yMa94zNgKfdQHihLuZ0s5d4rwj5N2zzjWlxgWsJrkfC+rs3XiekVMjB85vTBM89segIYAXTSGyMDLG3tXnPvaMUdvJlII++K16YMIEY/v00MI/FI+P3isnOZ2+4IHBmA1u419wNLgRJQ9vM8CyyTVDrRlgV75BKFDNRGLkdAmgNIo0NC8FrcQR3Dq2J4NX5E7BoND6xxj4fWeg3pdAJY7mHGj0ACyKE878S9ImFu+wCUMkh516Z+wxowUsUkTOzaHMckaDzOeZgDAgCq6kAmYRoMVeoMeSi1uLWhFSipL/KIvgkqlvvNIhKXquNi9e8jBGwd8xT1njW97TWjDeDKI2KOYhKXGOiG7Y36eIBsaUxAAaqmY56WkJCKedsyCzWWOs3mADO/46D9GtFmSjXQt3WI1yQAYDlCfaF5qIxAgIQpY/mNRo3MRiC4ljB6DSNikjXOjnsdLLKRCNQpe57xvW33HaPy9gtgT2MphQoOUB5spUDFV7cAtBgRkLDOUjky2x8jtXJbSMqIiokrjjdSA4FnRKLF5qXM+O+eQSrjWFYyAKrci3AF9i5ULA7iDe9PDOIZFxFEO8wMnKZ9//N1KeWDN880qAEA5SsEHjBqQCuQWhnSrYgyll6lXLCxPYQYZMzZzCwwNwpuJHRlqg58XT1KQKohgcB+GMYw5ABaJVFaQAlipo8Sr9AVSlTMWigdbYtpLS5jbLCPscmpMNoiYCWj9RCfgTxrqdgWMxoBw/tmJPzq1Vq7IFFuZ4MI+EGQug0ewIo1m+l64JtMZzopTl/h3IvfY3jwipdGboqZEfeJHDwzjYPCEkFLnvZkuWEEFGBFpjV/liBcoLSWwONxIoCbWr7BGIvEls/yvt0JNoh0k9v6GWTgZ0YaCeb4fia8MZgPptYVLVC9dBrcWWiWIiZMo2CRUtECNImE6RMaEyMSInBtdJz0Sv8ZDI+Oo0WHwP2JI14DAosy0JV3GDp7jLMnjjWMQGDY9b5ECtmf67UIystMJWENuB73JuiEhQSrUWh74vBzFIrrUIUu7Ku9TB19AdGG940UMteItQWQyX6O/elVSmMjTGd6IpGtjwDGXtzzuB8FvwbAn4QkKFp/St2060Gmhy9x4dTx6OwzNcLQc/tJ5DtxpoZQ6TxYych60GhR63v7j3xw7gTbN3Tzt8GVOLllDSOgiL1CxlMI5deABIBzuSwIzFSrKIQ123Yx2fMF8lRZl/4hve8cCkmoBCRz2OVxVLoFraxwHfAObQD3SaSn+/nR/k+yc12Rzd86DckcNKmBundgn4S/7GnRgcdTyQSbH/46qZZF9L78LKmWAuk932DIzjBOhs5PPcnG3I85/eZBbNsJh0ik0SLg5X8A1qgB8/zLL+2kvbCIRDoPKoBYt5WYVYLa8CKglGLDnkcYWHkftijWfW45VcfhilMIdK7OpGm786tsTqZ59/XfYjsOXas3UJspMdjf7wH3CGBEQCKlQzppNd2azJlAQMSbMdbd8XHKPV+kbLuGz6rVqGQ4O/kyWkth3/EVbm8pcu3k3+l4+CkSSpF/5Qec//chtNae3bAG5it1m7m5iNPzOBO1VKQtUI6RGK8qausfpX39Xi7W3Agtv//bbCv+ghOvvUS1Zs8ftYHXMhrm/B1ovFYfODEO82uDFpiqweVaIXg+UM0xuv1rfPSJpygUCnW25iBBEZug5xyB/4ZMVeF8xydY+8iBG1EPsN4yAuC+315wOm9EtfludC4i1TK6MnEjqg1t3YzMug7ExbIstNZMvHRDYb+u7fl09y/MWei6smrLDi6++xa6PBrdJtyEKKVQqSyrtu6kNE9VaLAXmk0Knz7AvXcdA+1cr+v8xEpwta2H0tzNNv6scj2tft1B/6K7541vTjI/n9StAx9aabobXXAgSti3aoyVqUl6y238oW/xbJ8Y57YbXUh5dOlFDv78Wc6fPM6du/dwz31f5tWRZc26129Lb7Ukrp6k9+R7SOE2jvzzCCtmeuekFyGggIoNVr4tOC+UXEss4fa77oFqid137+VMpRh+qIKm03ZkJU7UJimzCCvXRtkGK7dwBF5xdvDgQ118/vHH+NdInr9cW0qiFq72g9cmwErV6fkELCBZvXKc4ubdON5bWMqC5IIlmeIfEyt4bczdsbZlYaTvLLWaxeTkJM+/MQTZdhO3ZRIAoHruTZzCYlral9CSVuSV0JJQ0X9msbejyDfQRm9OkZ8k0vyZwJQjTGvFdFWYGhmEscu8fEhx4gPNi6c6oKWNJDXMN4gIAasyLQOHf62yy9dTtQXtvyIpD2YAUBk/BuLAm3xaDMAan9+8dvHOSimsZJpMNkttpkzFzvH0n/OQLUJrB6DI1gbtqQYEHGC6VM6MFlurHZXev6K0kGBhRSUzqHw7dr4dWrvQdJNvb4VUjoRlkVdTjJ9/fRT3D6VtEigBV8pHf/r7iV1P7sut2Npiz5SoVmuGQ5t4nwbPoT6dzAhEzoT3AigLqSRxKgp4HxRkMxlymQyl4QulytGfHAT6gGlz1CSwGtiBlfpYcsmWTZJoyTo3vWmzPJT6pl6WElYC5ZQq9tB7J9G1w8BbwEXANg2ngZVAN9Dp3f8vSRUYBi4DA959XR5YuP9gsw2e3WoRoAKUcb///n/IfwCA/cfu6DUO7AAAAABJRU5ErkJggg==' },
                text: { text: 'image', 'font-size': 9, display: '', stroke: '#000000', 'stroke-width': 0 }
            }
        })
    ],
    fsa: [
        new joint.shapes.fsa.StartState,
        new joint.shapes.fsa.EndState,
        new joint.shapes.fsa.State({ attrs: { text: { text: 'state' } } })
    ],
    pn: [
        new joint.shapes.pn.Place({ tokens: 3 }),
        new joint.shapes.pn.Transition({ attrs: { '.label': { text: 'transition' }} })
    ],
    erd: [
        new joint.shapes.erd.Entity({ attrs: { text: { text: 'Entity' } } }),
        new joint.shapes.erd.WeakEntity({ attrs: { text: { text: 'Weak entity', 'font-size': 10 } } }),
        new joint.shapes.erd.IdentifyingRelationship({ attrs: { text: { text: 'Relation', 'font-size': 8 } } }),
        new joint.shapes.erd.Relationship({ attrs: { text: { text: 'Relation' } } }),
        new joint.shapes.erd.ISA({ attrs: { text: { text: 'ISA' } } }),
        new joint.shapes.erd.Key({ attrs: { text: { text: 'Key' } } }),
        new joint.shapes.erd.Normal({ attrs: { text: { text: 'Normal' } } }),
        new joint.shapes.erd.Multivalued({ attrs: { text: { text: 'MultiValued', 'font-size': 10 } } }),
        new joint.shapes.erd.Derived({ attrs: { text: { text: 'Derived' } } })
    ],
    uml: [
        new joint.shapes.uml.Class({ name: 'Class', attributes: ['+attr1'], methods: ['-setAttr1()'], attrs: { '.uml-class-name-text': { 'font-size': 9 }, '.uml-class-attrs-text': { 'font-size': 9 }, '.uml-class-methods-text': { 'font-size': 9 } } }),
        new joint.shapes.uml.Interface({ name: 'Interface', attributes: ['+attr1'], methods: ['-setAttr1()'], attrs: { '.uml-class-name-text': { 'font-size': 9 }, '.uml-class-attrs-text': { 'font-size': 9 }, '.uml-class-methods-text': { 'font-size': 9 } } }),
        new joint.shapes.uml.Abstract({ name: 'Abstract', attributes: ['+attr1'], methods: ['-setAttr1()'], attrs: { '.uml-class-name-text': { 'font-size': 9 }, '.uml-class-attrs-text': { 'font-size': 9 }, '.uml-class-methods-text': { 'font-size': 9 } } }),
        new joint.shapes.uml.State({ name: 'State', events: ['entry/','create()'], attrs: { '.uml-state-name': { 'font-size': 10 }, '.uml-state-events': { 'font-size': 10 } } })
    ],
    org: [
        new joint.shapes.org.Member({ attrs: { '.rank': { text: 'Rank' }, '.name': { text: 'Name' }, image: { 'xlink:href': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAM8AAADBCAIAAADn6XN4AAAACXBIWXMAAAsTAAALEwEAmpwYAAAjPUlEQVR42u2dd3Rbx53vZ25BJQoL2HsRKaqREiWK6pZkSzIt+cmULHtXlhMr2T2J8zbOJntc1om98dk4G6fYfrJjZyMXuVuWrKhaVrF6oyhSVDE7KZJgBQsAAgRwy7w/AF6ARSwggHtB3c+ZwzP3EhjMDL743am/gQghIDIYR39fa3VZR8Pt9robVqPBbjHZrWa71eSMkFK5VKmWKtVShUqqUMvVYbrELF3SdF3ydG1kIoCQ7+wLFyiqzUnnne9rir9tun25tbrU0tPhXSIYTiTNXhoWm5q1eEPizAKI4XwXS1jc02qjHfY75Wdqio9VXT5iNrT4NnGJPCRt3qr0BWvS8lYr1OF8l1UQ3KNqM7Y3lhx+v/zYJ1ZTl78/CyckWYvXzyvcHp+dz3e5eeYeUxtCdaXflRzcWXP1GGKZIf8MjUkhpXJCIvMMGI7bLWa71dV0s/UZbX293n14VOqseYXbZ6zYRErlfFcEP9xDaqu9euzEey8ZGis9b5IyhTYqKTQ6WROdNE4RsDTVpa/paa0fktQ4kavCFj/2y7zC7RhB8l0lgeaeUFt7bfmJ915quH7Go9wwPC49JiNHFRELgJe9SIfV3HjjfGdjhRfvDY1JWbHtxelLHr6n+rBTXG3mrtZTH75y47svwUAxIcQiEqfFTpsnV4f55CMMjZX1pd8xNOXFe3WJWYXP/L/YaXP5rqcAMZXVdvPU7m//+qzNYnQVFcN1SVmx0+ZJ5SG+/SCTQV9x4QBiWS/eCyGWv/Gny594ASelfFeY35maaus39xx9+z9un/2au6PU6lLnrpSrQv30ie11N+7cOO/12yOTszf86p3I5BkBrqgAMwXVVl966uDrPzN3tbpKiOGx03Kj03OgP1tIiGVvnPzC0d/ndQoYKV2x9fn8R56GEAtwjQWMqaa2Ux++cmH369ylQhOePGe5XO0vk+ZJa3VZS2XJJBNJyb1v03/uImWKAGQ48EwdtTG048iOfy8//hl3Jyp1ZmzmPAgCZCqMnc21V49NPp2o1FmP/uZTVURsYLIdSKaI2mx9vXt+9+Sd8nPOSwzDE2cv0UYlBTIPdqvp+7P7fJKUKjzm0d98GpU2O5D5DwBTQW3G9sZdzxZyE50EKU3OXaHU6gKcDZu5p/LiIV+lRkikj770efKcZQEuhV8JerWZOps/evYhY0eT81IiD0nJXSFVqAKfE3NXW33pdz5MECckm17clZZ3f+DL4ieCW2193e0fP/dQd0ud81KhDk+as5TgaeDK0FTVWl3q2zSnmIULYrVZjYZPnn+YmzhSanXJOcuht9NQk6e+7LSl18uFcaNAyhSP//arqbF+JFjVZreYPvzVGkNTlfNSptQkz1nG4zw3Q1NVFw/6qTIlMuW21w5Hpszkq3S+IigHEhHL7P39U5zUSKkiYUYBxHGEWL5CT0ud/363Dpvls99s5sarg5egVNupj37HtcdxQhKfnY+TJGIZvgJDObr01X4tsqWnY++rP2RoB991PymCT20V5w9c/OoNZxxieFxWnkSmQCzLY+hprWcov+tAX1H87bvP8139k4LgOwMTw9BYefD1n3HLh2LS50iVanbYKtxAwlD2bn1NYD6r9MgHsRm5cx7YymN5J0Mw9RJYmvrb04u79bXOS21UYkRiFt+ZAq3VpZbezoB9HMTwp14/EZU6i+9ye0MwPUlPfvBfnNRImTIsLo3HboEzmAwtgZQaAACxzMHX/y/r1eJN3gmaJ6m+4mrx/r854xDCyKTpCAHE8PkMZRnK0OTN1oRJ0l534+JXby5+7Jc8lt07gsO2MZT90Jv/xu2S0kQlSuRKgFgeA8vQrdVlfNmYc1/8ybs9OPwSHLbNs3Il8hCNLp7fngEAoPNOxWTWTk4S589v2x8OB9d2/CCwbYbGygtf/MUZhxCLSMxECPDbYOttb7QaDfxWi77iasnh9/n+ciZGENi2U7teQci1wUQZGonjJOLVsFlNXcb2xrv9F0KYmZ2lVIUolAqVWtVv7e/qNLS3tre3tvk8J+c/f232qsckvt7U4z+ErjZ9xdWqy9844xBiIWFR/D5D+83d3JITTyCEc+blJKUmp6an4vgITzeT0VRafK28tNxus/kqM5Zew5V9f13y+H/wWCETQujjbZ88v4Hby6TURmqiEnjMjK3P2NNSO6TGSJKcmTMrr2C+WqMeMwXKQd28frP0SklPd49PsiRVqH7y95Jg8WojaLXVXTv5+W82uzIKoS45G+dvlYfdah4+9Z42LW3V2lUq9YQXb1bcqjh2+JjD7oP5rvz/89NVP3qFr2qZEIJW23s/v6+tttwZV2jC1RFxfOXEZjEZ2xu55iMAIEQVsnLNfRmZGV6n2Wfu+/bQt/W1DZPMG0aQP3uvLCQsmq/KGT/CVdud8nOfvPCwK5cQhidMw3BeDBvq626zGt2OtyCEOXlzlixfJJFKJp96eenN08fPOByTMnIFm5+578lf81E5E0O4atv76g8qzh9wxmUhGlU4DzveWIY2GfSUzcrdiYzS3f/gqqiYKB9+islo+vLjr0xGs9cpKDQRP3u/nJAI3beDQNVmNrS8tT2XZWjnpVoXF/h+Pm23mQx6Lg8Qwrz83MXLCzDM94OUFEUd2X+spqrW6xTW//vbs1ZuCXAVTRSBjoBc++YDj68ZIySyAI+x2Swma28n91OUK+RrC1enpCUBAIAffp8kQazfuPbc6YvFl655l0LJwb+LavMGhnaUfbOLuySlcoBQwGwwQsBqMtgtJu5OXEJs4Yb7Q1QhAPgxDxCCpSsKQsO0x4+eYpkJu0tqqbrWUnVN4M65hKi2qktHPJfxEFI565WzKi9ADG3p7aQpu/MSQjh/YW7BkvkYhnl2SP3HjFmZak3Iwa+P2mz2ib637OgugatNiPOklQOdAycEKQ3M9gLKZjF1tXBSUyjkGzc/uHjpAgxCgFDAQkJC7GNbHwkN1Uy03qouHeF3Tm9MBNdLYCj7X/5pGre8giClCk2Evz8UAWS3GB39Fu5OfELMg+tXKUN48zVks9kP7DvW3Di2X30I3V/iP7+6P2nWYr7yPCaCe5LWl57yXMmDEYS/J0ZZhrb19Xr2PRcszClYPA86TRpPyKSSok3rLpy7erW4fBSLACHMyc0uvXbLeVl54YCQ1Sa4J+lQxy0I+HXxkKO/z2rs4qSmUMg3blpTsHguAIj3ZegQA4uX5RU9uk6lvuvoz8rVixYuyuUGZaouHubxFzImwrJtLENXD6z4cMLQDoL1y+kCiGUc/X2e3pnjE2LWPbRcqVT4te85UeITop98quj8matlZbcR686YTCYt3LAyITEGAJCQGHOnQQ8AMBn0LdWlgu0rCEttrdWlQw5nYWiKoR2Yr1eo0g477ejnnlAQwgULZy8syIEQgoD0PScEgcPl981fsmxuXW1TfV2zVCpRqZU5udMxDHPmNi090ak2AEDt1eOi2sZF8/dXuDjX+HVY+yRypa+80bIsTdltnn03lUr5YOGymNhIAPwycusrcAzLyEjKyPBwgTiQ25Rk93oFzzoUGsJSm76imIvHxOpa9B0AAIRYh83qg9N6EKIpB8u4H50YBnPnZS8syCEIHAnp6TlRQtSKkBBFX58VANBSVYIQK0xX0cLKE/e7lEolK1ctwHFX9pyDYSxDed3iZiiHw2bxlJouMmzL4+uWLMklcMjv9i2fhJhYly9Ou8XUeceb42kCgIBsm7G9sa+73RmPjokID9PkzZ9x+dIN5x2EEO2wYziOYcT4fbQhBBBLs4O3nRIEnp8/a+48Z7sniE2aJzHREdVVd5xxfUVxZHI23zkaAQGprdnzMRoTDgCbnz8DIfbK5VvcfZZhWIaBEIMYBKM8LBAAiEUIDZ9uio3TrV27SKVyDtsKrkPgNTEx7oOU9BXFuWuf5DtHIyAgtbVVl3HxmOhwgBAEoGDhrIhw7bFjlymK5v6LEIsYAAAzcOAGBNA9aoEAGtFixcbqFi6cmZAQ5UyC7+L6GJ0uFMdxhmEAAK0eNSkoBKS27lb3XqbwcDVnltLT4yIj15SX19y+XW+zDVrjOjCEMVoTH0KQlZWclZXk1FlgJtcDD4aB0FCVwdALAOhprQcICfB0QAGprbfN1ewgSUIhl3qaH7VKsWTx7IKFM2tqmstv1La2jmvncHi4JisraXpWkkIhA2AK2rMhaDRKp9poh62vp12AOxUEozaEettdalOrlSOO5uM4zMxMyMxMYFlkMll6esw9veaeHnNPj9lmc0hIgiBxkiQjwtXR0eGxsRFSKbePYYrrzIlGreTiPW0NotruiqW3k1v+r9EoRh/QxyDQahRajSIFjLo/YIo+NO+GWu1esdLbdicheyHfORqKUNTW09bAxdUqpdDWQQUFg9XWwHd2RkAoauMeo8Bp26bQ2ETA0Kjd0y1cI1hQCEVttj4jF1fIJVO+Re8PFHL3Dj/P+hQOQlEbZXOvmyUJ7F5rcvkEknAPeVB2yyRS8hfCUZt7hzBBBvccOW9AgOMYw7AAAIdHfQoHoajN4Wnb8KkzfRlgSJJgGAcAgOoXbdvdGWTbxCept5AE7vQO57CLtu3ueKqNJLF7ZDzW55Cka5EzJT5JR4HxWHkG4ZSdzfQ3GObqKAjzQAWhqM1zaS5N0UDGm1fAoIaiXCv5iMkvdfYDQlGbROae46MoCgChO4cSJjTtUptErpxcSn5BKGojB6mNEfuk3sHZNs/6FA7CUZt7jo+maFFt3kFxtk3Km0+JURCi2hwULfYSvIBmWG41g2d9CgehqM2z3eZw0OIIiBc4HO7F9GK7bTRU4TFc3NzXL47ueoHZ7B5j86xP4SAUtWmjk7m4yWQT221eYDK7T5nxrE/hIBi1RSWCAQ9Wpj4bEte3TRyTuZ+La6OTJpGSvxCK2giJTBUWbe5qBc7fqGjbJo5o2yaANjrZqTaL1cHQNOeWQWScuNUGoTZKiLZNQN8oZ/wRclYcEsOEQq/J5TFYqdX5wEmPHxCQbdMlZnLxdoNFq5HxnaNgwmKlLFbX1m5dYhbf2RkZAaktLms+F2/rNGemhvKdo2CivdN9cFHc9PmTSMmPCEhtMRm5GEE6l8q0d1rEAd4J0d7pdo0dnyWqbcysSGRRKTNbq0sBAD1Gm8NBSUgfO0CdwrR1DiwNhzA2M4/v7IyMgNQGAIifPt+pNoRAe6c1PiZoTkznF4ZBhm7XYFt4XLpcJdBGiLDUFpc1v3j/35zxlo6++GghTi0LkA6DhRlwNx4n1McoENQICAAgafZSzg9UQ7OJ9zGFYAn1zSaPOlzC99d4V4Rl25RaXVzmPH3FVQBAj9Hea7Rr1T4433jKU9/s6pBiOJE+/wG+s3NXhKU2AEBmQaFTbQCAumZj7vRwvnMkdAw9tj6La89L4sxFgm20AaE9SQEAmYvWc/GG5j7eH1LCD5xhAwBkLnqI7y9wNARn20JjUiKTszsabgMAOrttFqtDKRdcJgVFfbNrpA1CLLOgkO/sjIbgbBsY/AOtqDMF8mzQoAvthv5ek2vCKjZzngD9UXoiRLXNWrmF65lW1BlZlgGAFcOI4Xat23PWjBWb+P7qxkCIDyltdHLKnGX1ZacBAJZ+pl5vSY0X4ip73um3M3VNrikEiTxk1sotfOdoDIRo2wAA8x76MRe/XWPm/YElzFBRZ+YGdWet3CJVqPj+3sZAiLYNAJCx4AFNZIKxowkA0GqwdRvtYRrRV8MgWAS+rxvojUI4t/ApvnM0NgK1bRDD5677AXdZXiX2FYaGuiaLpd+1Vzlp1mLBrmnzRKC2DQAw98Gnzn72B9phBwDUNFlnZ4SEqoWb2wDDsujqbfdsVZ5Hw0PICNS2AQCkSvW8B7c74wiBq7fFaVN3qGiwmi2uvcqRyTOmFTzI99c1LgRtLQoefabs24/sVjMA4E6rraPLHhkmtt4AzaCySvf8wYpt/ynMs2+HI+hcKtTh+Ruf5i6Lb4udUwQQulljtdpc+20VKelpC4Q7DT8EQasNALBg40+V2ghnvNVA1bfY0L2N1cZcr3Z7cH4j9Nrcj+Z+Xf01Aojv72pshK42iUyZ88hPuMsL5X126p6eWjh33UzRLmFVhlMNGrqso+yR/Y/k7srdW71X4JoTtNqslPX3V36/Sf/bLrmrq99vZy/ftPD+LOMr1DbbG9tcs6IMBg5luN3MXO+8XrS/KGdXzp7qPaxQffZAYZ5fRrHUzhs7f3vxt62WVgBAvIn412tqbCCna/JD4nWC7t/4A5sD7TllsjlctQAfUZVm0SdvdnaZHUNemaRO+tOKPxVlFPGd5aEIUW1lHWVbDm6p6qnyvLmuVrGk0bWfWSHDilaEeB60cy/wXYm1vtW1ahImktgLOoADFqHSeuN3tzoMwzS3NnntjlU70rRpfGfcjbDUZqWsvz7/6zeuvcEgxvN+Zqxq7fSoyDdNoMM1yJQcQ6ycK0TnA36ispE6f2PAzQcOsRd0MME9GMQiVNbQe/JWp8Fs93yXnJC/uPDFX+X9SoILYsG9gNR2tvns9m+3V/dUe95MilCsmROdEqkEAIBqB/vHTq4dnJcpmZ0miEr0Nx29zJFL/cxAYwwWquDD6uEvYxG63mA8eatjiOayw7P/uvqvy+KX8V0OYajNQlleOPfCjms7WA+3bXIJvmFebE6S1vOVaJ8JHXENbEIIHpgvjYuY4juc++3oH+dtVtvA15REYs/qAH7XVgSL0PnKruM32x20uzIhgD+Y+YM/LPtDhDyCx7Lwr7ZTTae2H91eZ6zzvDkvJbQwN0Y2fK88C9BbXeCW65kiJeH6xVKVfMo24FgEvrlsb+8Z0I0Ghy/ogGbsH1ivlTpwreV7vcnzplaqfef+d7Zk8rYMjk+1MYh56fxLv7v8O89RIo2C3JgXNy3m7ku1LCx6tRMYXA04jRIWLpRIySkoOATAqTKqoW2gCYtD+IsIkD6BxsNtvenAtRajddApRJumbdq5Zqdaoh5/Or6CN7Xp+/RF+4sut172vDk/NWzdnGjpmO4/minwWicYGAsIV8O180nJlJtBvXiLrmjyGDl7TAuWT3gNs4Nmj99sv1jdxXp80RmhGbvX756jmxPgEvGjtostF4v2FznH0pxoFZKNeXHpUeN2/FHaD/63m7vSaeGaeQQ5hcbgrlQytxo8pDZPDraHeZ1am9G2t7hZ3+N2zCsn5G+ufPNHs34UyELxoLadN3Y+feJpO+PuNy1IC3twdiyBT/BpeNYCP+/lrqJD4f1zMWJK9Bmu1bDX6zy+l1w5eipskvM+LEInbnWcqezw/MK3ZW97e/XbSjJA2z4CqjaKpX7x3S/eKnuLu4NjcH1ubF6Kt7/aE31wr3vTUaQWrMrFpMH8SEUIXKlE3zd6fCkzZehfwsBEf4p3oarN/NWVJqvDPZw5I3zG7g27p4dND0DpAqe2zv7Ozfs3n24+zd1RyYjHFyYlhE/KkRH8xgwPunteKgVYnQM0wblFi2bA6RugqdN9B2VJ0b+GA5/2gYxW6ovLjU3d7jlWGSH7vPDzh9Mf9ncBA6S2m4abq3evbre2c3fiwxSP5yeq5T4wRHB3LzzjXoQjJcF9s1G0cL1hjIzVDk6UwS6zx60UCftvEcAPE3QMi7692XahxsDdgQD+ecWfn5n3jF/LGAi1XWq9tHbPWqPd/cjLTQpdnxtHYD6rR3i6D9tr5MaGIQR56WhGIv8D1+Ok0wi+LcMo9zlVAM2Vs1tDfWvVhnC7xbSvpNlGuZ+qLy588ZXFr/jvE/2utnP6c4V7C00O18MOg3DtrOiFab4f0Ya3bNgH3cDuLk5CBFo0nZEJe3KLRaC8HitvwDy/B/SAii1UA/+PIVod9Htn6ztM7mM9fjLnJztW7cD8s/Tcv2o73Xz6ob0P9VEutyg4Bp8oSE7R+cu/KdRT+N+6QK/7xyqTgEVZTEKEQNd7Ga3w3G3cYPKQFQ7ZLVo2P3BOOfsdzCeXGjybcY9lPfbh2g/9MZHvR7WdaDyxYd8GK+UqhpzEty1KidX6eeGGkcHf74YNg5bfZCcwuSm0oAZHEAK3mvDSOoL1rH4pZH4cjtIDfcq5g2G/uNJY0+FuM65NXvvVhq98PjLiL7UdbTi68R8b+2nXcKJCgj+xKCVGE5A1QizATpjxb8yAcReNJMDMRDo7gSYEsFr5TideUkuY+wc9KdkcObNZC5T85I9h0dfXmm7q3W3rRbGLDm48GCrzZW/LL2o7XH+46B9FNsbVGlBKiW0FKVHqgJ7tApsp/ONu2E573lRIUW4KlRpFQ56mVTtN2NVaSadxsKRkGFOkYfN4dmmNEDh0Q3+1wT1DMyti1qV/uqQgfZYx36vt+J3j6/auo1nX1xwiJbYVpOhUfBwjxCBirxG7ZBmyNUQtZzPj6PQomiQC1GlFCDR1EZV6orV36OOcTZfST4bxZdKGc7Ki/Wx1B3e5Omn1oY2HfNWG87HarrZfXf7Fcq6tppKR2xamRIQEuiEyqITtNH7QiN22DblP4Cg1ks6Mo0KVfuxD2Bywuo2saiEt9qHmFEURzEMaNltwx3ldqjMcve2ewt46feuuB3dBX/SQfam2ZnNz/qf5LX0tzku1jHyyICVMyafUOLAaO37ACJup4f8KVTIJ4XRcGB2hYnz1hO2zYU1dRHM30d47uB8AAAAAqXBmjYrNVwp2y9uZ6o7vKt1D8c8teO7Vpa9OPlmfqc1CWZZ+vrS0o9R5KSXwf1maHqoQ1mAXfsWCHzVDIzPifwkcJYZTESomXMWEhTATGntGAJisWK8F77XiDZ2kqf8uOpJh9CoVsywECKmDPCKHbuhLGt1tuB2rdjyd8/Qk0gPAV2pjEVu0v2hfzT7nJY7BJ/JTEkMFOlsJmxz4eQt+vR/Qo5VdJWe1CkYlZ6QEInFEEkiCI5JANAMpBjpoSDHQTsF+B9ZjwXut+OgVyaZJmcVKdoZcsPZsaIYR+rKksarDNSyPQ3z3+t0bMzZOJk3fqO25s8/9z5X/4S4fnhM/O07w85QORJw0YxU2TE9NPrG7IoHMfAW9SoVUgrdmw6AY9qPL9fregRFTQn588/FFsYu8TtAHavvg1gc//OaH3OXiNN3KaYL2bD20CkwMVmHDv7dh1XZuPfCkwACbKGGzZMx0GYoJ5vVPAFgd9PuX6rotrsWIYbKwa09cS1IneZfaZNV2pvnM/V/d72BcY/dZUZqi3MRg3SOAAHbHgbVQsI3C2iislRqv+HDIRhIommSjCRRLsvESpAiS5+U46LE6PrhUa3G4hrSWxC05teUUDr0x1ZNSW6+9N2NnhqHftXAlRi3ftiCNxKdORcMOGtpYaGOBDUEbC+wstCFEQiDDkMz9l40ObgM2Jt1W+/9eqKYGdrS+VPDSy4te9iId79WGAFq3Z93RhqPOS5WU/GFBuiqoF86K3J3Spu7Dt5udcRzip7acWhI34cMCvbdD71x/h5MahPDR3GSVhOTbQ6gY/BVy48MyIzXOr5tBzNbDW3tsPRPVjJe2rbqnOvejXAvlWjG7alpMfpLOa+GKBAX9FLPzUpXJ5urCb562+cv1X04oBW9sG4OYbUe2cVJLDFUuSBSlNvWRk/iGmYncdMvuqt07b+ycUAre2Lb/vvzfL5570RmXENiP8qdp5MKaMxDxH2dq287Xu6btlaSyZGtJZljmON87YbWVdpQu/HQhN+RROD1+dqz3u2pFgg4WoY9LavVG15BvXlTela1XxjlnPzG12Whb3sd5t7puOS8zItRFs5P5Lr5IoDH2O94rrrbTrunmTws/fTzr8fG8cWLttpcvvsxJTSEh1mbF811wER7QyCX3pbmni5498yy3xmx08Jdffnmcn1FnrHvi8BOc18j12YnRqnvIO6SIJ1Eh8mqD2TnBYHKYSIxckbBizHdNwLY9f/Z5znnHjChtRria90EgMfAVIICr0mM4bbx29bVGU6PP1Hap9dLuyt3OOIlhy1Jj+C6vGHgOCdqQTJ3GKQkrZX327LO+URsC6Jenfsn59MuLjxCnDcQAEFiRGsM5PPii4ovz+vOjC2lcHs/2VO250HLBGVdKiAUJkXy7TxURBGqpJC9ed6mxAwCAAPr5dz+/8s9XRtlnP7ZtczCO588+z10uToySYBjvvyoxCCTkx0eGDLgFLWkv+azis1G0NLba3i57u6a3xhkPV8hmRofxXUAxCCiQOLY02T0a8serf/RebT22nlcuuZ3eLE+OxgDkv4hiEFLIjgwNlbt21pV1lJ1sPHk3OY3RbvtzyZ+7ba6NN4makJRQtdhiExlOXqzuWK1r9dtfSv6yMnHliC8bzbbZaNu71991xiEA96XG8l0oEYGSHamVD/jYPlx/uLK7csSXjaa2Tys+7ex3ueVMCVVHyGW8G20xCDMQEJsT5VqcwSL2jWtvjKio0Wblc3blXO+87owXZacmafzld01kCmCh6L+XfM8gBABQkIrGHzeGy8OHvOautu1U0ylOauFyWZI6hPcfkBiEHJQEkRWhdQrGSlnfLX93uKjuqjZPY5gbE8F3WcQQBCE3xr2E+62yt7hFkGOord5Yf6D2gDMuI/Dp4Vr+iyIGwQedXJaodjW3Wvpa9tfuH6KrkUdAdpTu4FYWzdSF4RATBz5ExkNOdESjyeVmeU/1nk3TNnn+dwS19VF97918zxnHIJwTGQFQsO5+FwkwSWq1FMftDAMAOFR3yEbbZITbQd0IT9JPvv+k197rjKeFakIkEr4ttBiCJmAQpmhdZ1GaHeZjd455SmsEtXHr2AAAOZER/JdADEEV0rUaTj97q/d6Smvok7Srv4s7ikotkcQolWKLTWRCJKpVJIZRLAsA2F+7n2IpEnMtEhlq2/bX7uccNKeFaibyKSIiAABAYFiSxnV0dret+3ST+xy9obbt65qvuXiqRiMaNhEvSNNqa3pcRy/sqd6zOmm1Mz7Itnk26xQEEaNU8t4IEEMwhmSVCh/w4LCvZh+LXK64Btm2I/VHbLTL03uKRgO4nQgiIhOBxPAElarBZAIAtFnaLrdeLogtAENsm2cPIs2jZyEiMlFSNGoufrH1ojPitm022na4/rAzLsXxOGWI2GgT8ZoohdujfHFbsTPiVtvxxuNmh+sUt2S12rUiXETEK8IkUm4cZAS1Hak/wsVT1BpRaSKTAkKdXN5isQAA6nrruvq7wuXh7nbb5dbLrpcBkBCi4r1fI4ZgD5Fy1+F/CKDi9mLA2bZ+ur+8s9wZD5PJCHHRh8ik0cndR00WtxWvTV7rUltpRynFuvypRsoVYotNZPJEygapDXC27UrrFfeL5ApRbCKTRyWRyHDCxtBgqNraPNQmE22biG+IlMsb+8wAgDZLW5O5aajaCAwLk8hEtYn4BJ1M4VQbAOCG4QYBADD0G+p66wb+LQcQimIT8QkqidvZvN6sJwAAxW3F3Iyo+BgV8SFK3H0SVaullQAeI20AAJ1MIY59iPgK+XC1lXWWcbcipaJtE/EZCtw9WdXS10IAADj/vATElKREFJuIr5DiBA6h012Dy7bp+/TO/ykIUjRsIr5FQZBmygGcts3O2DutLkdGSoIUG20ivkWOu9TWbm0n9H16rkOqJEgwvgOLRETGiYJwdRRolib0Zr37H7ho20R8jGdHgWjua+YulIREbLeJ+BY55h4EIZrNbrWJtk3Er2BchxQMHvkVEfEJnuZLtG0i/sXT1a673YZBKMPE8TYRP0J09Xc5YzKMAEAUm4iP8XxaEpyrNgmGi1oT8TmD2m0mh8kZIyEuNtpEfM6gdtug8xLuVbUhAFwmH3nEufto8GuGvBeNI+0JZMOHJfJzlsaXjINluDihlWqdB76YaPvl3mZXLSNXYgOVjgZuDsS5+2jwa1xvH/Ze7tVjpjnsvVx+BikAjFcZyH178H2PNEUCAxEmD3Oqzc7SFX0GvvMjMpXBUjWpfOdB5J4gQZUATzedvv+r+4cf2+FXMIhBACGEQyNg8B047M7wF8NhkcEvHi0FCDEwdgrDXwyhe6UMvPuqGc+XDf2Xr98lnJyM+C6VRPXcguf+P+owdGfsF2paAAAARHpUWHRDb21tZW50AAB42nNNySxJTVFIqlQISCzNUQjOSC3KTcxTSMsvUggPcM7JLEgsKtFRCChNyslMVnDJz03MzAMA0cYSC/gtqgsAAAAASUVORK5CYII=' } } })
    ]
};


//////////////////////////////////////// Rappid code

var Rappid = Backbone.View.extend({

    initialize: function(options) {        
        this.options = options || {};
        this.initializeEditor();
    },

    initializeEditor: function() {

        this.inspectorClosedGroups = {};

        this.initializePaper();
        this.initializeStencil();
        this.initializeSelection();
        this.initializeHaloAndInspector();
        this.initializeNavigator();
        this.initializeClipboard();
        this.initializeCommandManager();
        this.initializeToolbar();
        // Intentionally commented out. See the `initializeValidator()` method for reasons.
        // Uncomment for demo purposes.
        // this.initializeValidator();
        // Commented out by default. You need to run `node channelHub.js` in order to make
        // channels working. See the documentation to the joint.com.Channel plugin for details.
        //this.initializeChannel('ws://jointjs.com:4141');
        if (this.options.channelUrl) {
            this.initializeChannel(this.options.channelUrl);
        }
    },

    // Create a graph, paper and wrap the paper in a PaperScroller.
    initializePaper: function() {
        
        this.graph = new joint.dia.Graph;

        this.graph.on('add', function(cell, collection, opt) {
            if (opt.stencil) {
                this.createInspector(cell);
                this.commandManager.stopListening();
                this.inspector.updateCell();
                this.commandManager.listen();
                this.inspector.$('[data-attribute]:first').focus();
            }
        }, this);

        this.paper = new joint.dia.Paper({
            width: 400,
            height: 400,
            // Modified by me, initially was:
            // width: 1000,
            // height: 1000,
            gridSize: 10,
            model: this.graph,
            defaultLink: new joint.dia.Link({
                attrs: {
                    '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z', transform: 'scale(0.001)' }, // scale(0) fails in Firefox
                    '.marker-target': { d: 'M 10 0 L 0 5 L 10 10 z' },
                    '.connection': { stroke: 'black' }
                }
            }),
            linkConnectionPoint: joint.util.shapePerimeterConnectionPoint
        });

        this.paperScroller = new joint.ui.PaperScroller({
            autoResizePaper: true,
            padding: 0,
            paper: this.paper
        });

        this.paperScroller.$el.appendTo('.paper-container');

        this.paperScroller.center();

        this.graph.on('add', this.initializeLinkTooltips, this);

        $('.paper-scroller').on('mousewheel DOMMouseScroll', _.bind(function(evt) {

            if (_.contains(KeyboardJS.activeKeys(), 'alt')) {
                evt.preventDefault();
                var delta = Math.max(-1, Math.min(1, (evt.originalEvent.wheelDelta || -evt.originalEvent.detail)));
            var offset = this.paperScroller.$el.offset();
            var o = this.paperScroller.toLocalPoint(evt.pageX - offset.left, evt.pageY - offset.top);
                this.paperScroller.zoom(delta / 10, { min: 0.2, max: 5, ox: o.x, oy: o.y });
            }

        }, this));

        this.snapLines = new joint.ui.Snaplines({ paper: this.paper });
    },

    initializeLinkTooltips: function(cell) {

        if (cell instanceof joint.dia.Link) {

            var linkView = this.paper.findViewByModel(cell);
            new joint.ui.Tooltip({
                className: 'tooltip small',
                target: linkView.$('.tool-options'),
                content: 'Click to open Inspector for this link',
                left: linkView.$('.tool-options'),
                direction: 'left'
            });
        }
    },

    // Create and popoulate stencil.
    initializeStencil: function() {

        this.stencil = new joint.ui.Stencil({
            paper: this.paper, // WARNING: MODIFIED BY ME, ORIGINAL: this.paperScroller
            graph: this.graph, // WARNING: ADDED BY ME
            width: 240,
            groups: Stencil.groups,
            dropAnimation: true,
            search: {
                '*': ['type','attrs/text/text','attrs/.label/text'],
                'org.Member': ['attrs/.rank/text','attrs/.name/text']
            }
        });

        $('.stencil-container').append(this.stencil.render().el);

        this.stencil.$el.on('contextmenu', function(evt) { evt.preventDefault(); });
        $('.stencil-paper-drag').on('contextmenu', function(evt) { evt.preventDefault(); });

        var layoutOptions = {
            columnWidth: this.stencil.options.width / 2 - 10,
            columns: 2,
            rowHeight: 80,
            resizeToFit: true,
            dy: 10,
            dx: 10
        };

        _.each(Stencil.groups, function(group, name) {
            
            this.stencil.load(Stencil.shapes[name], name);
            joint.layout.GridLayout.layout(this.stencil.getGraph(name), layoutOptions);
            this.stencil.getPaper(name).fitToContent(1, 1, 10);

        }, this);

        this.stencil.on('filter', function(graph) {
            joint.layout.GridLayout.layout(graph, layoutOptions);
        });

        $('.stencil-container .btn-expand').on('click', _.bind(this.stencil.openGroups, this.stencil));
        $('.stencil-container .btn-collapse').on('click', _.bind(this.stencil.closeGroups, this.stencil));

        this.initializeStencilTooltips();
    },

    initializeStencilTooltips: function() {

        // Create tooltips for all the shapes in stencil.
        _.each(this.stencil.graphs, function(graph) {

            graph.get('cells').each(function(cell) {

                new joint.ui.Tooltip({
                    target: '.stencil [model-id="' + cell.id + '"]',
                    content: cell.get('type').split('.').join(' '),
                    left: '.stencil',
                    direction: 'left'
                });
            });
        });
    },

    initializeSelection: function() {
        
        this.selection = new Backbone.Collection;
        this.selectionView = new joint.ui.SelectionView({ paper: this.paper,  model: this.selection });

        // Initiate selecting when the user grabs the blank area of the paper while the Shift key is pressed.
        // Otherwise, initiate paper pan.
        this.paper.on('blank:pointerdown', function(evt, x, y) {

            if (_.contains(KeyboardJS.activeKeys(), 'shift')) {
                this.selectionView.startSelecting(evt, x, y);
            } else {
                this.selectionView.cancelSelection();
                this.paperScroller.startPanning(evt, x, y);
            }
        }, this);

        this.paper.on('cell:pointerdown', function(cellView, evt) {
            // Select an element if CTRL/Meta key is pressed while the element is clicked.
            if ((evt.ctrlKey || evt.metaKey) && !(cellView.model instanceof joint.dia.Link)) {
                this.selection.add(cellView.model);
                this.selectionView.createSelectionBox(cellView);
            }
        }, this);

        this.selectionView.on('selection-box:pointerdown', function(evt) {
            // Unselect an element if the CTRL/Meta key is pressed while a selected element is clicked.
            if (evt.ctrlKey || evt.metaKey) {
                var cell = this.selection.get($(evt.target).data('model'));
                this.selection.reset(this.selection.without(cell));
                this.selectionView.destroySelectionBox(this.paper.findViewByModel(cell));
            }
        }, this);

        // Disable context menu inside the paper.
        // This prevents from context menu being shown when selecting individual elements with Ctrl in OS X.
        this.paper.el.oncontextmenu = function(evt) { evt.preventDefault(); };

        KeyboardJS.on('delete, backspace', _.bind(function(evt, keys) {

            if (!$.contains(evt.target, this.paper.el)) {
                // remove selected elements from the paper only if the target is the paper
                return;
            }

            this.commandManager.initBatchCommand();
            this.selection.invoke('remove');
            this.commandManager.storeBatchCommand();
            this.selectionView.cancelSelection();

            // Prevent Backspace from navigating one page back (happens in FF).
            if (_.contains(keys, 'backspace') && !$(evt.target).is("input, textarea")) {

                evt.preventDefault();
            }

        }, this));
    },

    createInspector: function(cellView) {

        var cell = cellView.model || cellView;

        // No need to re-render inspector if the cellView didn't change.
        if (!this.inspector || this.inspector.options.cell !== cell) {

            // Is there an inspector that has not been removed yet.
            // Note that an inspector can be also removed when the underlying cell is removed.
            if (this.inspector && this.inspector.el.parentNode) {

                this.inspectorClosedGroups[this.inspector.options.cell.id] = _.map(this.inspector.$('.group.closed'), function(g) {
            // MODIFIED BY ME, INITIALLY WAS:
            //    this.inspectorClosedGroups[this.inspector.options.cell.id] = _.map(app.inspector.$('.group.closed'), function(g) {
            return $(g).attr('data-name');
        });
                
                // Clean up the old inspector if there was one.
                this.inspector.updateCell();
                this.inspector.remove();
            }

            var inspectorDefs = InspectorDefs[cell.get('type')];

            this.inspector = new joint.ui.Inspector({
                inputs: inspectorDefs ? inspectorDefs.inputs : CommonInspectorInputs,
                groups: inspectorDefs ? inspectorDefs.groups : CommonInspectorGroups,
                cell: cell
            });

            this.initializeInspectorTooltips();
            
            this.inspector.render();
            $('.inspector-container').html(this.inspector.el);

            if (this.inspectorClosedGroups[cell.id]) {

        _.each(this.inspectorClosedGroups[cell.id], this.inspector.closeGroup, this.inspector);

            } else {
                this.inspector.$('.group:not(:first-child)').addClass('closed');
            }
        }
    },

    initializeInspectorTooltips: function() {
        
        this.inspector.on('render', function() {

            this.inspector.$('[data-tooltip]').each(function() {

                var $label = $(this);
                new joint.ui.Tooltip({
                    target: $label,
                    content: $label.data('tooltip'),
                    right: '.inspector',
                    direction: 'right'
                });
            });
            
        }, this);
    },

    initializeHaloAndInspector: function() {

        this.paper.on('cell:pointerup', function(cellView, evt) {

            if (cellView.model instanceof joint.dia.Link || this.selection.contains(cellView.model)) return;

            // In order to display halo link magnets on top of the freetransform div we have to create the
            // freetransform first. This is necessary for IE9+ where pointer-events don't work and we wouldn't
            // be able to access magnets hidden behind the div.
            var freetransform = new joint.ui.FreeTransform({ cellView: cellView, allowRotation: false });
            var halo = new joint.ui.Halo({ cellView: cellView });

            // As we're using the FreeTransform plugin, there is no need for an extra resize tool in Halo.
            // Therefore, remove the resize tool handle and reposition the clone tool handle to make the
            // handles nicely spread around the elements.
            halo.removeHandle('resize');
            halo.changeHandle('clone', { position: 'se' });
            
            freetransform.render();
            halo.render();

            this.initializeHaloTooltips(halo);

            this.createInspector(cellView);

            this.selectionView.cancelSelection();
            this.selection.reset([cellView.model]);
            
        }, this);

        this.paper.on('link:options', function(evt, cellView, x, y) {

            this.createInspector(cellView);
        }, this);
    },

    initializeNavigator: function() {

        var navigator = this.navigator = new joint.ui.Navigator({
            width: 240,
            height: 115,
            paperScroller: this.paperScroller,
            zoomOptions: { max: 5, min: 0.2 }
        });

        navigator.$el.appendTo('.navigator-container');
        navigator.render();
    },

    initializeHaloTooltips: function(halo) {

        new joint.ui.Tooltip({
            className: 'tooltip small',
            target: halo.$('.remove'),
            content: 'Click to remove the object',
            direction: 'right',
            right: halo.$('.remove'),
            padding: 15
        });
        new joint.ui.Tooltip({
            className: 'tooltip small',
            target: halo.$('.fork'),
            content: 'Click and drag to clone and connect the object in one go',
            direction: 'left',
            left: halo.$('.fork'),
            padding: 15
        });
        new joint.ui.Tooltip({
            className: 'tooltip small',
            target: halo.$('.clone'),
            content: 'Click and drag to clone the object',
            direction: 'left',
            left: halo.$('.clone'),
            padding: 15
        });
        new joint.ui.Tooltip({
            className: 'tooltip small',
            target: halo.$('.unlink'),
            content: 'Click to break all connections to other objects',
            direction: 'right',
            right: halo.$('.unlink'),
            padding: 15
        });
        new joint.ui.Tooltip({
            className: 'tooltip small',
            target: halo.$('.link'),
            content: 'Click and drag to connect the object',
            direction: 'left',
            left: halo.$('.link'),
            padding: 15
        });
        new joint.ui.Tooltip({
            className: 'tooltip small',
            target: halo.$('.rotate'),
            content: 'Click and drag to rotate the object',
            direction: 'right',
            right: halo.$('.rotate'),
            padding: 15
        });
    },

    initializeClipboard: function() {

        this.clipboard = new joint.ui.Clipboard;
        
        KeyboardJS.on('ctrl + c', _.bind(function() {
            // Copy all selected elements and their associated links.
            this.clipboard.copyElements(this.selection, this.graph, { translate: { dx: 20, dy: 20 }, useLocalStorage: true });
        }, this));
        
        KeyboardJS.on('ctrl + v', _.bind(function() {

            this.selectionView.cancelSelection();

            this.clipboard.pasteCells(this.graph, { link: { z: -1 }, useLocalStorage: true });

            // Make sure pasted elements get selected immediately. This makes the UX better as
            // the user can immediately manipulate the pasted elements.
            this.clipboard.each(function(cell) {

                if (cell.get('type') === 'link') return;

                // Push to the selection not to the model from the clipboard but put the model into the graph.
                // Note that they are different models. There is no views associated with the models
                // in clipboard.
                this.selection.add(this.graph.getCell(cell.id));
        this.selectionView.createSelectionBox(cell.findView(this.paper));

            }, this);

        }, this));

        KeyboardJS.on('ctrl + x', _.bind(function() {

            var originalCells = this.clipboard.copyElements(this.selection, this.graph, { useLocalStorage: true });
            this.commandManager.initBatchCommand();
            _.invoke(originalCells, 'remove');
            this.commandManager.storeBatchCommand();
            this.selectionView.cancelSelection();
        }, this));
    },

    initializeCommandManager: function() {

        this.commandManager = new joint.dia.CommandManager({ graph: this.graph });

        KeyboardJS.on('ctrl + z', _.bind(function() {

            this.commandManager.undo();
            this.selectionView.cancelSelection();
        }, this));
        
        KeyboardJS.on('ctrl + y', _.bind(function() {

            this.commandManager.redo();
            this.selectionView.cancelSelection();
        }, this));
    },

    initializeValidator: function() {

        // This is just for demo purposes. Every application has its own validation rules or no validation
        // rules at all.
        
        this.validator = new joint.dia.Validator({ commandManager: this.commandManager });

        this.validator.validate('change:position change:size add', _.bind(function(err, command, next) {

            if (command.action === 'add' && command.batch) return next();

            var cell = command.data.attributes || this.graph.getCell(command.data.id).toJSON();
            var area = g.rect(cell.position.x, cell.position.y, cell.size.width, cell.size.height);

            if (_.find(this.graph.getElements(), function(e) {

            var position = e.get('position');
                var size = e.get('size');
            return (e.id !== cell.id && area.intersect(g.rect(position.x, position.y, size.width, size.height)));

            })) return next("Another cell in the way!");
        }, this));

        this.validator.on('invalid',function(message) {
            
            $('.statusbar-container').text(message).addClass('error');

            _.delay(function() {

                $('.statusbar-container').text('').removeClass('error');
                
            }, 1500);
        });
    },

    initializeToolbar: function() {

        this.initializeToolbarTooltips();
        
        $('#btn-undo').on('click', _.bind(this.commandManager.undo, this.commandManager));
        $('#btn-redo').on('click', _.bind(this.commandManager.redo, this.commandManager));
        $('#btn-clear').on('click', _.bind(this.graph.clear, this.graph));
        $('#btn-svg').on('click', _.bind(this.openAsSVG, this));
        $('#btn-png').on('click', _.bind(this.openAsPNG, this));
        $('#btn-zoom-in').on('click', _.bind(function() { this.paperScroller.zoom(0.2, { max: 5, grid: 0.2 }); }, this));
        $('#btn-zoom-out').on('click', _.bind(function() { this.paperScroller.zoom(-0.2, { min: 0.2, grid: 0.2 }); }, this));
        $('#btn-zoom-to-fit').on('click', _.bind(function() {
            this.paperScroller.zoomToFit({
                padding: 20,
                scaleGrid: 0.2,
                minScale: 0.2,
                maxScale: 5
            });
        }, this));
        $('#btn-fullscreen').on('click', _.bind(this.toggleFullscreen, this));
        $('#btn-print').on('click', _.bind(this.paper.print, this.paper));

        // toFront/toBack must be registered on mousedown. SelectionView empties the selection
        // on document mouseup which happens before the click event. @TODO fix SelectionView?
        $('#btn-to-front').on('mousedown', _.bind(function(evt) { this.selection.invoke('toFront'); }, this));
        $('#btn-to-back').on('mousedown', _.bind(function(evt) { this.selection.invoke('toBack'); }, this));

        $('#btn-layout').on('click', _.bind(this.layoutDirectedGraph, this));
        
        $('#input-gridsize').on('change', _.bind(function(evt) {
            var gridSize = parseInt(evt.target.value, 10);
            $('#output-gridsize').text(gridSize);
            this.setGrid(gridSize);
        }, this));

        $('#snapline-switch').change(_.bind(function(evt) {
            if (evt.target.checked) {
                this.snapLines.startListening();
            } else {
                this.snapLines.stopListening();
            }
        }, this));

        var $zoomLevel = $('#zoom-level');
        this.paper.on('scale', function(scale) {
            $zoomLevel.text(Math.round(scale * 100));
        });
    },

    initializeToolbarTooltips: function() {
        
        $('.toolbar-container [data-tooltip]').each(function() {
            
            new joint.ui.Tooltip({
                target: $(this),
                content: $(this).data('tooltip'),
                top: '.toolbar-container',
                direction: 'top'
            });
        });
    },

    toggleFullscreen: function() {

        var el = document.body;

        function prefixedResult(el, prop) {
            
            var prefixes = ['webkit', 'moz', 'ms', 'o', ''];
            for (var i = 0; i < prefixes.length; i++) {
                var prefix = prefixes[i];
                var propName = prefix ? (prefix + prop) : (prop.substr(0, 1).toLowerCase() + prop.substr(1));
                if (!_.isUndefined(el[propName])) {
                    return _.isFunction(el[propName]) ? el[propName]() : el[propName];
                }
            }
        }

        if (prefixedResult(document, 'FullScreen') || prefixedResult(document, 'IsFullScreen')) {
            prefixedResult(document, 'CancelFullScreen');
        } else {
            prefixedResult(el, 'RequestFullScreen');
        }
    },

    openAsSVG: function() {

        this.paper.toSVG(function(svg) {
            var lightbox = new joint.ui.Lightbox({
            title: '(Right-click, and use "Save As" to save the diagram in SVG format)',
                image: 'data:image/svg+xml,' + encodeURIComponent(svg)
            }).open();
        });
    },

    openAsPNG: function() {

        this.paper.toPNG(function(dataURL) {
            var lightbox = new joint.ui.Lightbox({
            title: '(Right-click, and use "Save As" to save the diagram in PNG format)',
                image: dataURL
            }).open();
        });
    },

    setGrid: function(gridSize) {

        this.paper.options.gridSize = gridSize;
        
        var backgroundImage = this.getGridBackgroundImage(gridSize);
        this.paper.$el.css('background-image', 'url("' + backgroundImage + '")');
    },

    getGridBackgroundImage: function(gridSize, color) {

        var canvas = $('<canvas/>', { width: gridSize, height: gridSize });

        canvas[0].width = gridSize;
        canvas[0].height = gridSize;

        var context = canvas[0].getContext('2d');
        context.beginPath();
        context.rect(1, 1, 1, 1);
        context.fillStyle = color || '#AAAAAA';
        context.fill();

        return canvas[0].toDataURL('image/png');
    },

    layoutDirectedGraph: function() {

        this.commandManager.initBatchCommand();
        
        _.each(this.graph.getLinks(), function(link) {

            // Reset vertices.
            link.set('vertices', []);
            
            // Remove all the non-connected links.
            if (!link.get('source').id || !link.get('target').id) {
                link.remove();
            }
        });

        var pad = 0; // padding for the very left and very top element.
        joint.layout.DirectedGraph.layout(this.graph, {
            setLinkVertices: false,
//            rankDir: 'LR',
            rankDir: 'TB',
            setPosition: function(cell, box) {
                cell.position(box.x - box.width / 2 + pad, box.y - box.height / 2 + pad);
            }
        });

        // Scroll to the top-left corner as this is the initial position for the DirectedGraph layout.
        this.paperScroller.el.scrollLeft = 0;
        this.paperScroller.el.scrollTop = 0;
        
        this.commandManager.storeBatchCommand();
    },

    initializeChannel: function(url) {
        // Example usage of the Channel plugin. Note that this assumes the `node channelHub` is running.
        // See the channelHub.js file for furhter instructions.

        var room = (location.hash && location.hash.substr(1));
        if (!room) {
            room = joint.util.uuid();
            this.navigate('#' + room);
        }

        var channel = this.channel = new joint.com.Channel({ graph: this.graph, url: url || 'ws://localhost:4141', query: { room: room } });
        console.log('room', room, 'channel', channel.id);

        var roomUrl = location.href.replace(location.hash, '') + '#' + room;
        $('.statusbar-container .rt-colab').html('Send this link to a friend to <b>collaborate in real-time</b>: <a href="' + roomUrl + '" target="_blank">' + roomUrl + '</a>');
    }
});

var rappid = new Rappid();

});
