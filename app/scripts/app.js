define(['jquery', 'underscore', 'backbone', 'bootstrap'], 
    function($, _, Backbone, jquery){
        'use strict';
        var app = {};

        app.init = function(){
            Backbone.history.start();

            // enable modals
            $(function(){
            	//$('#loginModal').modal('show');
            	// $('#registerModal').modal("show");
                //$('#loginModal').on('shown.bs.modal', function(){alert('Modal shown');});
                //$('#registerModal').on('shown.bs.modal', function(){alert('Modal shown');});
            });
        };

        return app;
    }
);
