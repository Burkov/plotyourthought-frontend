/*global require*/
'use strict';

require.config({
    paths: {
        jquery: '/static/scripts/vendor/jquery',
        backbone: '/static/scripts/vendor/backbone',
        underscore: '/static/scripts/vendor/lodash',
        bootstrap: '/static/scripts/vendor/bootstrap',
        modernizr: '/static/scripts/vendor/modernizr',
        KeyboardJS: '/static/scripts/vendor/keyboard',
        handlebars: '/static/scripts/vendor/handlebars',
        mustache: '/static/scripts/vendor/mustache',
        geometry: '/static/scripts/vendor/geometry',
        Vectorizer: '/static/scripts/vendor/vectorizer',
        joint: '/static/scripts/vendor/joint',
        fabric: '/static/scripts/vendor/fabric',
        FabricEditor: '/static/scripts/fabric_editor',        
        MDConverter: '/static/scripts/vendor/Markdown.Converter',
        MDEditor: '/static/scripts/vendor/Markdown.Editor',
        MDExtra: '/static/scripts/vendor/Markdown.Extra',
        MDSanitizer: '/static/scripts/vendor/Markdown.Sanitizer'
    },    
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        },
        modernizr: {
            exports: 'Modernizr'
        },
        MDConverter: {
            exports: 'Markdown'
        },
        MDEditor: {
            deps: ['MDConverter']
        },
        MDExtra: {
            deps: ['MDConverter']
        },
        MDSanitizer: {
            deps: ['MDConverter']
        }
        
//        joint: {
//            deps: 'KeyboardJS',
//            exports: 'joint'
//        }
    }
});

require(['jquery', 'underscore', 'backbone', 'bootstrap', 'KeyboardJS', 'handlebars', 'mustache', 'joint', 'fabric', 'FabricEditor', 'MDConverter', 'MDEditor', 'MDExtra', 'MDSanitizer'], function ($, _, Backbone, bootstrap, KeyboardJS, Handlebars, Mustache, joint, fabric, EditorView, Markdown, MDEditor, MDExtra, MDSanitizer) {

    var PostFragmentModel = Backbone.Model.extend({

    });

    var PostFragmentsCollection = Backbone.Collection.extend({
        model: PostFragmentModel,
        url: '/api/post/'
    });

    var PostFragmentView = Backbone.View.extend({
        className: 'row',
        template: ' \
            <div class="wmd-panel col-lg-6"> \
              <div id="wmd-button-bar{{ postfix }}"></div> \
                <textarea class="wmd-input" id="wmd-input{{ postfix }}"> \
                </textarea> \
              </div> \
            <div id="wmd-preview{{ postfix }}" class="wmd-panel wmd-preview col-lg-6"></div> \
        ',
        initialize: function(options){
        },
        render: function(){
            var html = Mustache.to_html(this.template, {
                postfix: this.model.id || this.model.cid
            });
            this.$el.html(html);

            // set up Pagedown
            var converter = new Markdown.Converter();
            Markdown.Extra.init(converter);

            converter = Markdown.getSanitizingConverter();

            var editor = new Markdown.Editor(converter, this.model.id || this.model.cid);

            editor.run();

            return this;
        }

    });

    var postFragmentsCollection = new PostFragmentsCollection();

    postFragmentsCollection.fetch({
        success: function(data){
            for (var i=0; i<postFragmentsCollection.length; i++){
                if (postFragmentsCollection.at(i).attributes.content_type == "markdown") {
                    var postFragmentView = new PostFragmentView({model: postFragmentsCollection.at(i)});
                    $('.blog-post').append(postFragmentView.$el);
                    postFragmentView.render();
                }
                else {
                    var editorView = new EditorView();
                }
            }
        }
    });
});