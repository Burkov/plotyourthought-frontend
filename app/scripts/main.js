/*global require*/
'use strict';

require.config({
    paths: {
        app: 'app',
        
        jquery: '/static/scripts/vendor/jquery',
        backbone: '/static/scripts/vendor/backbone',
        underscore: '/static/scripts/vendor/lodash',
        bootstrap: '/static/scripts/vendor/bootstrap',
        modernizr: '/static/scripts/vendor/modernizr',
        KeyboardJS: '/static/scripts/vendor/KeyboardJS',
        joint: '/static/scripts/vendor/joint.all'
    },    
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        },
        modernizr: {
            exports: 'Modernizr'
        },
        joint: {
            deps: 'KeyboardJS',
            exports: 'joint'
        }
    }
});

require(['app'], function (app) {
    app.init();
});
