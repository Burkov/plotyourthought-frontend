define(['jquery', 'underscore', 'backbone', 'bootstrap', 'modernizr', 'fabric'], function($, _, Backbone, bootstrap, Modernizr, fabric) {

'use strict';

var ToolbarView = Backbone.View.extend({
    className: 'toolbar-container col-lg-12',
    template: '<div class="btn-toolbar" role="toolbar"> \
                 <div class="btn-group" role="group"> \
                   <button id="btn-save" class="btn btn-default"><i class="fa fa-save"></i></button> \
                   <button id="btn-clear" class="btn btn-default"><i class="fa fa-trash"></i></button> \
                 </div> \
                 <div class="btn-group" role="group"> \
                   <button id="btn-zoom-in" class="btn btn-default"><i class="fa fa-search-plus"></i></button> \
                   <button id="btn-zoom-out" class="btn btn-default"><i class="fa fa-search-minus"></i></button> \
                   <span id="zoom-level">100</span> \
                   <span>%</span> \
                 </div> \
                 <div class="btn-group" role="group"> \
                   <button id="btn-bring-forward" class="btn btn-default" data-tooltip="Bring Object to Front"><i class="fa fa-angle-up"></i></button> \
                   <button id="btn-send-backwards" class="btn btn-default" data-tooltip="Send Object to Back"><i class="fa fa-angle-down"></i></button> \
                   <button id="btn-to-front" class="btn btn-default" data-tooltip="Bring Object Forward"><i class="fa fa-angle-double-up"></i></button> \
                   <button id="btn-to-back" class="btn btn-default" data-tooltip="Send Object Backwards"><i class="fa fa-angle-double-down"></i></button> \
                   <button id="btn-group" class="btn btn-default"><i class="fa fa-object-group"></i></button> \
                   <button id="btn-ungroup" class="btn btn-default"><i class="fa fa-object-ungroup"></i></button> \
                 </div> \
               </div> \
              ',
    initialize: function(options){
        this.canvasView = options.canvasView;
        this.render();
    },
    render: function(){
        this.$el.html(this.template);
        return this;
    },
    events: {
        "click #btn-save": "onSave",
        "click #btn-clear": "onClear",
        "click #btn-zoom-in": "onZoomIn",
        "click #btn-zoom-out": "onZoomOut",
        "click #btn-bring-forward": "onBringForward",
        "click #btn-send-backwards": "onSendBackwards",
        "click #btn-to-front": "onToFront",
        "click #btn-to-back": "onToBack",
        "click #btn-group": "onGroup",
        "click #btn-ungroup": "onUngroup"
    },

    //event handlers
    onSave: function(){},
    onClear: function(){
        var activeGroup = this.canvasView.canvas.getActiveGroup();
        if (activeGroup){
            activeGroup.forEachObject(_.bind(function(obj){
                this.canvasView.canvas.remove(obj);
            }, this));
            this.canvasView.canvas.discardActiveGroup().renderAll();
        }
        else {
            var selected = this.canvasView.canvas.getActiveObject();
            if (selected){
                this.canvasView.canvas.remove(selected);
            }
        }
    },
    setZoom: function(scaleFactor){
        // stolen from: 
        // http://stackoverflow.com/questions/30732710/fabricjs-zoom-canvas-in-viewport-possible
        // http://jsfiddle.net/tornado1979/39up3jcm/

        var dimensions = { width: this.canvasView.canvas.getWidth() * scaleFactor,
                           height: this.canvasView.canvas.getHeight() * scaleFactor };

        this.canvasView.canvas.setDimensions(dimensions);

        var objects = this.canvasView.canvas.getObjects();
        for (var i in objects){
            objects[i].scaleX = objects[i].scaleX * scaleFactor;
            objects[i].scaleY = objects[i].scaleY * scaleFactor;
            objects[i].left = objects[i].left * scaleFactor;
            objects[i].top = objects[i].top * scaleFactor;
        }

        this.canvasView.canvas.renderAll();
    },
    onZoomIn: function(){
        this.setZoom(1.2);
    },
    onZoomOut: function(){
        this.setZoom(1 / 1.2);
    },
    onBringForward: function(){
        var activeGroup = this.canvasView.canvas.getActiveGroup();
        if (activeGroup){
            activeGroup.forEachObject(_.bind(function(obj){
                this.canvasView.canvas.bringForward(obj);
            }, this));
        }
        else{
            var selected = this.canvasView.canvas.getActiveObject();
            if (selected !== null){
                this.canvasView.canvas.bringForward(selected);
            }            
        }
    },
    onSendBackwards: function(){
        var activeGroup = this.canvasView.canvas.getActiveGroup();
        if (activeGroup){
            activeGroup.forEachObject(_.bind(function(obj){
                this.canvasView.canvas.sendBackwards(obj);
            }, this));
        }
        else {
            var selected = this.canvasView.canvas.getActiveObject();
            if (selected !== null){
                this.canvasView.canvas.sendBackwards(selected);
            }
        }
    },
    onToFront: function(){
        var activeGroup = this.canvasView.canvas.getActiveGroup();
        if (activeGroup){
            activeGroup.forEachObject(_.bind(function(obj){
                this.canvasView.canvas.bringToFront(obj);
            }, this));
        }
        else {
            var selected = this.canvasView.canvas.getActiveObject();
            if (selected !== null){
                this.canvasView.canvas.bringToFront(selected);
            }
        }

    },
    onToBack: function(){
        var activeGroup = this.canvasView.canvas.getActiveGroup();
        if (activeGroup){
            activeGroup.forEachObject(_.bind(function(obj){
                this.canvasView.canvas.sendToBack(obj);
            }, this));
        }
        else {
            var selected = this.canvasView.canvas.getActiveObject();
            if (selected !== null){
                this.canvasView.canvas.sendToBack(selected);
            }
        }
    },
    onGroup: function(){},
    onUngroup: function(){}
});

var MenuView = Backbone.View.extend({
    template: '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"> \
\
                 <div class="panel panel-default"> \
                   <div class="panel-heading" role="tab" id="headingMain"> \
                     <h4 class="panel-title"> \
                       <a role="button" data-toggle="collapse" data-parent="#accordion" href="#mainTools" aria-expanded="true" aria-controls="collapseOne"> \
                         Main tools \
                       </a> \
                     </h4> \
                   </div> \
                   <div id="mainTools" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne"> \
                     <div class="panel-body"> \
                       <div class="btn-group-vertical" role="group"> \
                         <button class="btn btn-default" id="btn-text"><i class="fa fa-font"></i><BR>Text</button> \
                         <button class="btn btn-default" id="btn-image"><i class="fa fa-file-image-o"></i><BR>Image</button> \
                         <button class="btn btn-default" id="btn-video"><i class="fa fa-file-video-o"></i><BR>Video</button> \
                         <button class="btn btn-default" id="btn-arrow">Arrow</button> \
                       </div> \
                     </div> \
                   </div> \
                 </div> \
\
                 <div class="panel panel-default"> \
                   <div class="panel-heading" role="tab" id="headingBasic"> \
                     <h4 class="panel-title"> \
                       <a role="button" data-toggle="collapse" data-parent="#accordion" href="#basic" aria-expanded="true" aria-controls="collapseOne"> \
                         Basic shapes \
                       </a> \
                     </h4> \
                   </div> \
                   <div id="basic" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"> \
                     <div class="panel-body"> \
                       <div class="btn-group-vertical" role="group"> \
                         <button class="btn btn-default" id="btn-circle"><svg width="20" height="20" fill="white" viewBox="0 0 20 20"><circle cx="10" cy="10" r="5"></circle></svg><BR>Circle</button> \
                         <button class="btn btn-default" id="btn-ellipse"><svg width="20" height="20" fill="white" viewBox="0 0 20 20"><ellipse cx="10" cy="10" rx="6" ry="4"></ellipse><BR>Ellipse</svg></button> \
                         <button class="btn btn-default" id="btn-line"><svg width="20" height="20" stroke="white" viewBox="0 0 20 20"><line x1="4" y1="4" x2="16" y2="16"></line></svg><BR>Line</button> \
                         <button class="btn btn-default" id="btn-polygon"><svg width="20" height="20" fill="white" viewBox="0 0 20 20"><polygon points="10,4 16,10 10,16 4,10"></polygon></svg><BR>Polygon</button> \
                         <button class="btn btn-default" id="btn-polyline"><svg width="20" height="20" fill="white" viewBox="0 0 20 20"><polyline points="4,4 8,16 12,6 16,8"></polyline></svg><BR>Polyline</button> \
                         <button class="btn btn-default" id="btn-rect"><svg width="20" height="20" fill="white" viewBox="0 0 20 20"><rect x="4" y="4" width="12" height="12"></rect></svg><BR>Rectangle</button> \
                         <button class="btn btn-default" id="btn-triangle"><svg width="20" height="20" fill="white" viewBox="0 0 20 20"><polygon points="10,4 16,16 4,16"></polygon></svg><BR>Triangle</button> \
                      </div> \
                     </div> \
                   </div> \
                 </div> \
\
                 <div class="panel panel-default"> \
                   <div class="panel-heading" role="tab" id="headingChess"> \
                     <h4 class="panel-title"> \
                       <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#chess" aria-expanded="false" aria-controls="collapseTwo"> \
                         Chess shapes \
                       </a> \
                     </h4> \
                   </div> \
                   <div id="chess" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo"> \
                     <div class="panel-body"> \
                       Content of the second panel \
                     </div> \
                   </div> \
                 </div> \
               </div>',
    className: 'menu-container col-lg-2',
    initialize: function(options){
        this.canvasView = options.canvasView;
        this.render();
    },
    render: function(){
        this.$el.html(this.template);
        return this;
    },
    events: {
        "click #btn-text": "onText",
        "click #btn-image": "onImage",
        "click #btn-video": "onVideo",        
        "click #btn-arrow": "onArrow",

        "click #btn-circle": "onCircle",
        "click #btn-ellipse": "onEllipse",
        "click #btn-line": "onLine",
        "click #btn-polygon": "onPolygon",
        "click #btn-polyline": "onPolyline",
        "click #btn-rect": "onRect",
        "click #btn-triangle": "onTriangle"
    },
    onText: function(){
        var text = new fabric.IText("Hello, World!", {
            backgroundColor: '#FFFFFF',
            fill: '#000000',
            fontSize: 12,
            left: 100,
            top: 100
        });
        this.canvasView.canvas.add(text);
    },
    onImage: function(){


        fabric.Image.fromURL(url, function(image){
            canvas.add(image);
        });
    },
    onVideo: function(){},
    onArrow: function(){},

    onCircle: function(){
        var circle = new fabric.Circle({
            radius: 20,
            fill: 'green',
            left: 100,
            top: 100
        });
        this.canvasView.canvas.add(circle);
        //var setCoords = circle.setCoords.bind(circle);
        //circle.on({
        //    moving: setCoords,
        //    scaling: setCoords,
        //    rotating: setCoords
        //});
        //this.canvasView.canvas.renderAll();
    },
    onEllipse: function(){
        var ellipse = new fabric.Ellipse({
            rx: 20,
            ry: 10,
            fill: 'red',
            left: 100,
            top: 100
        });
        this.canvasView.canvas.add(ellipse);
    },
    onLine: function(){
        var line = new fabric.Line([100, 100, 200, 200], {
            stroke: 'blue',
            strokeWidth: 2
        });
        this.canvasView.canvas.add(line);
    },
    onPolygon: function(){},
    onPolyline: function(){},
    onRect: function(){
        var rect = new fabric.Rect({
            left: 100,
            top: 100,
            width: 100,
            height: 100,
            fill: 'gray'
        });
        this.canvasView.canvas.add(rect);
    },
    onTriangle: function(){}
});

var CanvasView = Backbone.View.extend({
    className: 'fabric-container col-lg-8', // NOTE: ".canvas-container" selector is reserved for fabric.js
    initialize: function(){
        this.render();
    },
    render: function(){
        var nativeCanvas = document.createElement('canvas');
        nativeCanvas.setAttribute('id', 'editorCanvas');
        nativeCanvas.width = 800;
        nativeCanvas.height = 600;

        this.$el.append(nativeCanvas);        
        this.canvas = new fabric.Canvas(nativeCanvas, {
            backgroundColor: 'rgb(255,255,255)'
        });

        return this;
    },
});

var InspectorView = Backbone.View.extend({
    template: ' \
        <div> \
            <label>angle:</label> \
            <input class="inspector-attr form-control" data-fabricattr="angle" type="number"></input> \
        </div> \
        <div> \
            <label>originX:</label> \
            <input class="inspector-attr form-control" data-fabricattr="originX" type="text"></input> \
        </div> \
        <div> \
            <label>originY:</label> \
            <input class="inspector-attr form-control" data-fabricattr="originY" type="text"></input> \
        </div> \
        <div> \
            <label>scaleX:</label> \
            <input class="inspector-attr form-control" data-fabricattr="scaleX" type="text"></input> \
        </div> \
        <div> \
            <label>scaleY:</label> \
            <input class="inspector-attr form-control" data-fabricattr="scaleY" type="text"></input> \
        </div> \
        <div> \
            <label>stroke:</label> \
            <input class="inspector-attr form-control" data-fabricattr="stroke" type="string"></input> \
        </div> \
        <div> \
            <label>strokeWidth:</label> \
            <input class="inspector-attr form-control" data-fabricattr="strokeWidth" type="number"></input> \
        </div> \
        <div> \
            <label>fill:</label> \
            <input class="inspector-attr form-control" data-fabricattr="fill" type="string"></input> \
        </div> \
        <div> \
            <label>opacity:</label> \
            <input class="inspector-attr form-control" data-fabricattr="opacity" type="number"></input> \
        </div> \
    ',
    className: 'inspector-container col-lg-2',
    initialize: function(options){
        this.canvasView = options.canvasView;

        _.bindAll(this, 'inspectorValueChanged', 'canvasObjectModified', 'selectionChanged')

        this.canvasView.canvas.on('object:selected', this.selectionChanged)
                              .on('group:selected', this.selectionChanged)
                              .on('path:created', this.selectionChanged)
                              .on('selection:cleared', this.selectionChanged);

        // Attach delegated event handlers to $el so that
        // if user changes values of inputs in inspector,
        // these event handlers change selected element on canvas apropriately
        this.$el.on('change', '.inspector-attr', this.inspectorValueChanged);

        // Attach event handlers to canvas so that
        // if user changes the active element on canvas,
        // these event handlers change the inspector values apropriately
        this.canvasView.canvas.on("object:modified", this.canvasObjectModified);
        this.canvasView.canvas.on("object:moving", this.canvasObjectModified);
        this.canvasView.canvas.on("object:rotating", this.canvasObjectModified);
        this.canvasView.canvas.on("object:scaling", this.canvasObjectModified);

        this.render();

    },
    render: function(){
    },
    inspectorValueChanged: function(event){
        var attribute = event.target.dataset.fabricattr;
        var selected = this.canvasView.canvas.getActiveObject();
        var value = {};
        value[attribute] = $(event.target).val();
        selected.set(value);
        this.canvasView.canvas.renderAll();
    },
    canvasObjectModified: function(event){
        var selected = this.canvasView.canvas.getActiveObject();
        if ( (selected !== null) && (selected !== undefined) ) {
            _.each(this.$('.inspector-attr').toArray(), function(Element, index, list){
                var attribute = Element.dataset.fabricattr;
                var value = selected.get(attribute);
                $(Element).val(value);
            }, this);
            this.canvasView.canvas.renderAll();                
        }
    },
    selectionChanged: function(){
        // clear previous selection's event handlers if available

        // check type of new selection and create the appropriate inspector
        var selected = this.canvasView.canvas.getActiveObject();

        if ( (selected === null) && (this.canvasView.canvas.getActiveGroup() === null) ){
            this.noSelection();
        }
        else if ( (selected === null) && (this.canvasView.canvas.getActiveGroup() !== null) ){
            this.group();
        }
        else if (selected.type == 'line') {
            this.line();
        }
        else if (selected.type == 'circle'){
            this.circle();
        }
        else if (selected.type == 'triangle'){
            this.triangle();
        }
        else if (selected.type == 'ellipse'){
            this.ellipse();
        }
        else if (selected.type == 'rect'){
            this.rect();
        }
        else if (selected.type == 'polyline'){
            this.polyline();
        }
        else if (selected.type == 'polygon'){
            this.polygon();
        }
        else if ( (selected.type == 'i-text') || (selected.type == 'text') ){
            this.text();            
        }
        else if (selected.type == 'image'){
            this.image();
        }
        else if (selected.type == 'path'){
            this.path();
        }
        else {
        }

        // set values of inspector inputs
        if ( (selected !== null) && (selected !== undefined) ) {
            _.each(this.$('.inspector-attr').toArray(), function(Element, index, list){
                var attribute = Element.dataset.fabricattr;
                var value = selected.get(attribute);
                $(Element).val(value);
            }, this);
        }
    },
    noSelection: function(){
        this.$el.html('');
    },
    group: function(){
        this.$el.html('');
        // this.$el.html(this.template);
    },
    line: function(){
        var template = '\
        <div> \
            <label>x1:</label> \
            <input class="inspector-attr form-control" data-fabricattr="x1" type="number"></input> \
        </div> \
        <div> \
            <label>y1:</label> \
            <input class="inspector-attr form-control" data-fabricattr="y1" type="number"></input> \
        </div> \
        <div> \
            <label>x2:</label> \
            <input class="inspector-attr form-control" data-fabricattr="x2" type="number"></input> \
        </div> \
        <div> \
            <label>y2:</label> \
            <input class="inspector-attr form-control" data-fabricattr="y2" type="number"></input> \
        </div> \
        ' + this.template;
        this.$el.html(template);
    },
    circle: function(){
        this.$el.html(this.template);
    },
    triangle: function(){
        this.$el.html(this.template);        
    },
    ellipse: function(){
        this.$el.html(this.template);        
    },
    rect: function(){
        this.$el.html(this.template);        
    },
    polyline: function(){
        this.$el.html(this.template);
    },
    polygon: function(){
        this.$el.html(this.template);
    },
    text: function(){
        this.$el.html(this.template);        
    },
    image: function(){
        this.$el.html(this.template);
    },
    path:function(){
        this.$el.html(this.template);
    }

});

var EditorView = Backbone.View.extend({
    template: '<div class="row editor-toolbar-container"> \
               </div> \
               <div class="row editor-container"> \
               </div>',
    initialize: function(){
        this.render();
    },
    render: function(){
        this.$el.html(this.template);
        this.canvasView = new CanvasView();
        this.toolbarView = new ToolbarView({canvasView: this.canvasView});        
        this.menuView = new MenuView({canvasView: this.canvasView});
        this.inspectorView = new InspectorView({canvasView: this.canvasView});
        this.$('.editor-toolbar-container').append(this.toolbarView.el)
        this.$('.editor-container').append(this.menuView.el);
        this.$('.editor-container').append(this.canvasView.el);
        this.$('.editor-container').append(this.inspectorView.el);
        return this;
    }
});

return EditorView;

});