'use strict';
var LIVERELOAD_PORT = 35729;

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to match all subfolders:
// 'test/spec/**/*.js'
// templateFramework: 'lodash'

module.exports = function (grunt) {
    // show elapsed time at the end
    require('time-grunt')(grunt);
    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        app: 'app',
        dist: 'dist',
        banner: '/*!\n' +
            ' * <%= pkg.name %> v<%= pkg.version %>\n' +
            ' * Homepage: <%= pkg.homepage %>\n' +
            ' * Copyright 2012-<%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
            ' * Licensed under <%= pkg.license %>\n' +
            ' * Based on Bootstrap\n' +
            '*/\n',
        theme: 'superhero',
        watch: {
            options: {
                nospawn: true,
                livereload: LIVERELOAD_PORT
            },
            scripts: {
                files: [
                    'Gruntfile.js',
                    '<%= app %>/scripts/{,*/}*.js',
                    '!<%= app %>/scripts/vendor/*',
                    'test/spec/{,*/}*.js'
                ],
                tasks: ['default']
            },
            sass: {
                files: ['<%= app %>/styles/{,*/}*.{scss,sass}'],
                tasks: ['default']
            },
            test: {
                files: ['<%= app %>/scripts/{,*/}*.js', 'test/spec/**/*.js'],
                tasks: ['test:true']
            }
        },
        clean: {
            dist: ['.tmp', '<%= dist %>/*']
        },
        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: false
            },
            dist: {
                src: [],
                dest: ''
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc',  // globals are defined there
                reporter: require('jshint-stylish'),
                force: true, // continue even upon warnings
                undef: true, // warn on undefined variables
            },
            all: [
                'Gruntfile.js',
                '<%= app %>/scripts/{,*/}*.js',
                '!<%= app %>/scripts/vendor/*',
                'test/spec/{,*/}*.js'
            ]
        },
        mocha: {
            all: {
                options: {
                    run: true,
                    urls: ['http://localhost:<%= connect.test.options.port %>/index.html']
                }
            }
        },
        sass: {
          options: {
            style: 'expanded',
            precision: 8,
            //sourcemap: 'auto',
            //includePaths: ['.tmp/styles/bootstrap/'] // includePaths is only for libsass, it points to folder, where @import declarations should look for files.
            loadPath: ['.tmp/styles/bootstrap/', '.tmp/styles/font-awesome']
          },
          dist: {
            files: [{
              expand: true,
              cwd: '.tmp/styles/scss',
              src: ['main.scss'],
              dest: '.tmp/styles',
              ext: '.css'
            }]
          },
        },
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= app %>/images',
                    src: '{,*/}*.{png,jpg,jpeg}',
                    dest: '<%= dist %>/images'
                }]
            }
        },
        cssmin: {
            dist: {
                files: {
                    '<%= dist %>/styles/main.css': [
                        '.tmp/styles/{,*/}*.css',
                        '<%= app %>/styles/{,*/}*.css'
                    ]
                }
            }
        },
        htmlmin: {
            dist: {
                options: {
                    /*removeCommentsFromCDATA: true,
                    // https://github.com/yeoman/grunt-usemin/issues/44
                    //collapseWhitespace: true,
                    collapseBooleanAttributes: true,
                    removeAttributeQuotes: true,
                    removeRedundantAttributes: true,
                    useShortDoctype: true,
                    removeEmptyAttributes: true,
                    removeOptionalTags: true*/
                },
                files: [{
                    expand: true,
                    cwd: '<%= app %>',
                    src: '*.html',
                    dest: '<%= dist %>'
                }]
            }
        },
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= app %>',
                    dest: '<%= dist %>',
                    src: [
                        'styles/*.css',
                        '*.{ico,txt}',
                        'images/{,*/}*.{webp,gif,svg}',
                        'styles/fonts/{,*/}*.*',
                    ]
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/bootstrap-sass-official/assets/fonts/bootstrap',
                    src: '*.*',
                    dest: '<%= dist %>/fonts'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/font-awesome/fonts',
                    src: '*.*',
                    dest: '<%= dist %>/fonts'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/hover/css',
                    src: 'hover.css',
                    dest: '<%= dist %>/styles'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/requirejs',
                    src: 'require.js',
                    dest: '<%= dist %>/scripts/vendor'
                },
                {
                    expand: true, 
                    cwd: '<%= app %>/bower_components/jquery/dist',
                    src: 'jquery.js',
                    dest: '<%= dist %>/scripts/vendor'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/lodash',
                    src: 'lodash.js',
                    dest: '<%= dist %>/scripts/vendor'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/backbone/',
                    src: 'backbone.js',
                    dest: '<%= dist %>/scripts/vendor'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/mustache.js/',
                    src: 'mustache.js',
                    dest: '<%= dist %>/scripts/vendor'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/bootstrap/dist/js',
                    src: 'bootstrap.js',
                    dest: '<%= dist %>/scripts/vendor'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/modernizr/',
                    src: 'modernizr.js',
                    dest: '<%= dist %>/scripts/vendor'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/fabric.js/dist/',
                    src: 'fabric.require.js',
                    dest: '<%= dist %>/scripts/vendor/',
                    rename: function(dest, src){
                        return dest + 'fabric.js';
                    }
                },
                {
                    expand: true,
                    cwd: '<%= app %>/bower_components/pagedown/',
                    src: ['Markdown.Converter.js',
                          'Markdown.Editor.js',
                          'Markdown.Extra.js',
                          'Markdown.Sanitizer.js'],
                    dest: '<%= dist %>/scripts/vendor/'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/scripts/vendor/',
                    src: 'keyboard.js',
                    dest: '<%= dist %>/scripts/vendor'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/scripts/vendor/',
                    src: 'handlebars.js',
                    dest: '<%= dist %>/scripts/vendor'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/scripts/vendor/',
                    src: 'joint.js',
                    dest: '<%= dist %>/scripts/vendor'
                },
                {
                    expand: true,
                    cwd: '<%= app %>/scripts/',
                    src: '{,*/}*.js',
                    dest: '<%= dist %>/scripts'
                }],

            },
            bootstrap: {
                files: [{
                    expand: true,
                    cwd: 'app/bower_components/bootstrap-sass-official/assets/stylesheets/',
                    src: '**',
                    dest: '.tmp/styles/scss/'
                }]
            },
            bootswatchTheme: {
                files: [{
                    expand: true,
                    cwd: 'app/bower_components/bootswatch/<%= theme %>/',
                    src: '*.scss',
                    dest: '.tmp/styles/scss/bootstrap'
                }]
            },
            customTheme: {
                files: [{
                    expand: true,
                    cwd: 'app/styles/firebathero/',
                    src: '*.scss',
                    dest: '.tmp/styles/scss/bootstrap'
                }]
            },
            fontAwesome: {
                files: [{
                    expand: true,
                    cwd: 'app/bower_components/font-awesome/scss',
                    src: '*.scss',
                    dest: '.tmp/styles/scss/font-awesome'
                }]
            },
            main: {
                files: [{
                    expand: true,
                    cwd: 'app/styles/',
                    src: 'main.scss',
                    dest: '.tmp/styles/scss/'
                }]
            },
            css: {
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: ['*.css', '*.css.map'],
                    dest: '<%= dist %>/styles',
                }]
            }
        },
        rev: {
            dist: {
                files: {
                    src: [
                        '<%= dist %>/scripts/{,*/}*.js',
                        '<%= dist %>/styles/{,*/}*.css',
                        '<%= dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
                        '/styles/fonts/{,*/}*.*',
                        'bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*.*'
                    ]
                }
            }
        },
        exec: {
            collectstatic: {
                cwd: '../../',
                cmd: 'python manage.py collectstatic --clear --noinput'
            }
        }
    });

    grunt.registerTask('test', function (isConnected) {
        isConnected = Boolean(isConnected);
        var testTasks = [
                'clean:dist',
                'sass',
                //'mocha',
            ];

        if(!isConnected) {
            return grunt.task.run(testTasks);
        } else {
            // already connected so not going to connect again, remove the connect:test task
            testTasks.splice(testTasks.indexOf('connect:test'), 1);
            return grunt.task.run(testTasks);
        }
    });

    grunt.registerTask('build_scss', 'build a regular theme from scss', function() {
        //var theme = theme === undefined ? grunt.config('theme') : theme;
        // // cancel the build (without failing) if this directory is not a valid theme
        // var isValidTheme = grunt.file.exists(app, bower_components, bootswatch, theme, '_variables.scss') && grunt.file.exists(app, bower_components, bootswatch, theme, '_bootswatch.scss');
        // if (!isValidTheme) { return; }

        // copy bootstrap sources from bower to .tmp/styles/scss
        grunt.task.run('copy:bootstrap');
        // overwrite bootstrap variables with bootswatch theme variables
        // I replaced the bootswatchTheme with my custom theme
        //grunt.task.run('copy:bootswatchTheme'); 
        grunt.task.run('copy:customTheme');
        // copy font-awesome scss
        grunt.task.run('copy:fontAwesome');
        // copy bootswatch 
        grunt.task.run('copy:main');
        // build css from scss there
        grunt.task.run('sass:dist');
        // copy results to .tmp/styles
        grunt.task.run('copy:css');
    });

    grunt.registerTask('build', [
        'clean:dist',
        'build_scss',
        'imagemin',
        'htmlmin',
        //'cssmin',
        'copy:dist',
        //'rev',
    ]);

    grunt.registerTask('default', [
        'jshint',
        'test',
        'build',
        'exec:collectstatic'
    ]);
};
